package com.tbldevelopment.askthebible;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class OptionsFragment extends Fragment {
	 
	private LayoutInflater lflater;
	int requestFor=0;
	String[] textEntries ;
	private Typeface mFace;
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {
	    View view = inflater.inflate(R.layout.right_fragment_list,
	        container, false);
	    mFace = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Aver.ttf");
	    
	    Bundle bundle = this.getArguments();
	    if(getArguments()!=null) {
	        requestFor = getArguments().getInt("requestFor");
	    }
	    if(requestFor!=0)
	    {
	    	 lflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	 LinearLayout ll = (LinearLayout)view.findViewById(R.id.layoutOption);
	 	    for ( int i = 0; i < Constant.BIBLEVERSION.length; i++)
	 	 	{	
	 	    	//System.out.println(textEntries[i]);
	 	    	final LinearLayout holder = (LinearLayout) lflater.inflate(R.layout.cell_layout, null);
	 	 	    TextView txtTitle = (TextView) holder.findViewById(R.id.txtTitle);
	 	 	    holder.setTag(Constant.BIBLEVERSION[i]);
	 	 	    txtTitle.setText(Constant.BIBLEVERSION[i]);
	 	 	    txtTitle.setTypeface(mFace);
	 	 	    if (i== (Constant.BIBLEVERSION.length -1)) {
	 				((LinearLayout)holder.findViewById(R.id.cell_seprator)).setVisibility(View.GONE);
	 			}
	 	        ll.addView(holder);
	 	        holder.setOnClickListener(new View.OnClickListener() {
	 				
	 				@Override
	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					int index = (Integer) v.getTag();
						if (v.getTag().toString().equals("0")
								|| v.getTag().toString().equals("3")) {
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setData(Uri
									.parse("market://details?id=com.PandoraTV"));
								// Market (Google play) app seems not installed,
								// let's try to open a webbrowser
								intent.setData(Uri
										.parse("https://play.google.com/store/apps/details?id=com.PandoraTV"));

													} else {
							if (requestFor == Constant.SETUP) {
								Intent intent = new Intent(Constant.BROADCAST);
								intent.putExtra("TAG", String.valueOf(index));
								intent.putExtra("title",
										Constant.SETUPNINFO[index]);
								intent.putExtra("PANE", Constant.SETUPINFO);
								
								LocalBroadcastManager.getInstance(getActivity())
								.sendBroadcast(intent);
							}
						}
	 					
	 				}
	 			});
	 	       
	 	 	}
	    }
	    else{
	    	Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=com.PandoraTV"));
			startActivity(intent);
	    }
	    return view;
	 	}
	
	 }

