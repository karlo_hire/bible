package com.tbldevelopment.askthebible;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;

public class Utility extends Application {
	private static String PREFERENCE;
	public static Context appContext;

	
	
	public static Typeface getTypeFace(Context ctx)
	{
		Typeface typeFace = Typeface.createFromAsset(ctx.getAssets(),
				"fonts/verdana.ttf");
		return typeFace;
	}
	// for setting string preferences
	public static void setSharedPreference(Context context, String name,
			String value) {
		appContext = context;
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static void setSharedPreference3(Context context, String name,
			String value) {
		appContext = context;
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.putString(name, value);
		editor.commit();
	}

	public static void setSharedPreferenceBoolean(Context context, String name,
			Boolean value) {
		appContext = context;
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(name, value);
		editor.commit();
	}

	public static void setSharedPreference(Context context, String name, int id) {
		appContext = context;
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(name, id);
		editor.commit();
	}

	public static String getSharedPreferences(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getString(name, null);
	}

	public static int getSharedPreferences1(Context context, String name) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getInt(name, 0);
	}

	public static Boolean getSharedPreferences2(Context context,
			String signin_status) {
		SharedPreferences settings = context
				.getSharedPreferences(PREFERENCE, 0);
		return settings.getBoolean(signin_status, false);
	}
	
	public static String[] values = new String[]{"Relationships and Sex","Health and your Body","Money and Work","Feelings, Emotions and Attitude","God and Spirituality","Keywords and Phrases","Bible","Setup and Info"};
}
