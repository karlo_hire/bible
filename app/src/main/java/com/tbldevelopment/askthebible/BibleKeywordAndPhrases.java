package com.tbldevelopment.askthebible;

import java.util.ArrayList;

import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

public class BibleKeywordAndPhrases extends Activity {
	private Context mContext;
	private ArrayList<String> listkeywords;
	private LinearLayout layoutProgress;
	private ListView lv;
	private boolean keywordloaded = false;
	private ApplicationStringAdapter adapterString;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_keyword_phrases);
		mContext = this;
		ActionBar actionbar = getActionBar();
		actionbar.setTitle("Alpha Topic");
		listkeywords = new ArrayList<String>();
		lv = (ListView) findViewById(R.id.listViewbiblesKeyword);
		
		layoutProgress = (LinearLayout)findViewById(R.id.layoutProgress);
		
		//String Adapter
		adapterString = new ApplicationStringAdapter(mContext, 2, R.layout.cell_layout, listkeywords);
	
		lv.setAdapter(adapterString);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				String character = listkeywords.get(arg2);
				Intent in = new Intent(mContext, TopicByAlphaScreenActivity.class);
				in.putExtra("alpha", character);
				startActivity(in);
			}
		});
		
//		editSearchText = (AutoCompleteTextView)findViewById(R.id.editAutoCompleteSearch);
//		editSearchText.setTypeface(Utility.getTypeFace(mContext));
//		editSearchText.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//					long arg3) {
//				// TODO Auto-generated method stub
//				String keyWord = listkeywords.get(arg2);
//				String key = bibleadapter.getBiblesFromKeyword(keyWord);
//				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//		        imm.hideSoftInputFromWindow(editSearchText.getWindowToken(), 0);
//				if(key!=null)
//				new SearchContentOfTopic().execute(key);
//			}
//		});
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(!keywordloaded){
			layoutProgress.setVisibility(View.VISIBLE);
			
			Thread th = new Thread(loadingKeyword);
			th.start();
		}
	}
	
	private Runnable loadingKeyword = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			String [] bibles_title = getResources().getStringArray(R.array.alpha_topic);
			for (String key : bibles_title) {
				listkeywords.add(key);
			}
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					layoutProgress.setVisibility(View.GONE);
					keywordloaded = true;
					adapterString.notifyDataSetChanged();
					lv.setVisibility(View.VISIBLE);
				}
			});
		}
	};
	
//	public class SearchContentOfTopic extends AsyncTask<String, Void, String> {
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
////			mProgressDialog = ProgressDialog.show(mContext, "Loading Content",
////					"Searching...");
////			mProgressDialog.setCanceledOnTouchOutside(false);
////			mProgressDialog.setCancelable(true);
////			mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
////				
////				@Override
////				public void onCancel(DialogInterface arg0) {
////					// TODO Auto-generated method stub
////					SearchContentOfTopic.this.cancel(true);
////				}
////			});
//			layoutProgress.setVisibility(View.VISIBLE);
//			lv.setVisibility(View.GONE);
//			
//			adapterString.clear();
//		}
//
//		@Override
//		protected String doInBackground(String... params) {
//			// TODO Auto-generated method stub
//			//keys exa. {Rom.8.12,Gen.4.2}
//			String key = params[0];
//			if(key==null)
//				return null;
//			String[] keys = key.split(",");// spliting by the comma
//			ArrayList<HashMap<String, String>> listSearch = new ArrayList<HashMap<String, String>>();
//			if (keys.length > 0) {
//				for (int i = 0; i < keys.length; i++) {
//					HashMap<String, String> map = new HashMap<String, String>();
//					if (keys[i].contains("-")) {// check if contain exam {Rom.8.12-Rom.8.16}
//						String[] keys2 = keys[i].split("-");
//						String[] keysection1 = keys2[0].split("\\.");
//						String[] keySection = keys2[1].split("\\.");
//						String section = null;
//						
//						int from = Integer.parseInt(keysection1[2].replace("\\", ""));
//						int to = Integer.parseInt(keySection[2].replace("\\", ""));
//						for (int n = from; n <= to; n++) {
//							HashMap<String, String> mapsection = new HashMap<String, String>();
//							mapsection.put("book", keysection1[0]);
//							if (section == null) {
//								section = "{" + keySection[1];
//							} 
//							
//							section = section + ":" + n + "}";
//							mapsection.put("section", section);
//							section = null;
//							listSearch.add(mapsection);
//							mapsection = null;
//						}
//				
//					} else {
//						 String strkey = keys[i];
//						 String[] keySection =strkey.split("\\.");
//						 map.put("book", keySection[0]);
//						String section = null;
//						for (int n = 1; n < keySection.length; n++) {
//							if (section == null) {
//								section = "{" + keySection[n].replace("\\", "");
//							} else {
//								section = section + ":" + keySection[n].replace("\\", "") + "}";
//							}
//						}
//						map.put("section", section);
//						
//						listSearch.add(map);
//					}
//
//					
//				}
//				
//			}
//			BibleDbAdapter adapter = new BibleDbAdapter(mContext);
//			for (int i = 0; i < listSearch.size(); i++) {
//				HashMap<String, String> map = listSearch.get(i);
//				HashMap<String, String> dataMap = adapter.getBibleFromChapter(
//						mapkeys.get(map.get("book")), map.get("section"));
//				if(dataMap!=null)
//				listOfBibles.add(dataMap);
//			}
//			String result = "Success";
//			return result;
//		}
//
//		protected void onPostExecute(String result) {
//			layoutProgress.setVisibility(View.GONE);
//			lv.setVisibility(View.VISIBLE);
//			
//			if (result != null) {
//				if (result.contains("Success")) {
//		
//					adapterBibles.notifyDataSetChanged();
//				}
//				// adapter.notifyDataSetChanged();
//			}
//		}
//
//	}
}
