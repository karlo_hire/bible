package com.tbldevelopment.askthebible.tablet;

public class BaseBackPressedListener {

	public interface OnBackPressedListener {
		public void doBack();
	}

}
