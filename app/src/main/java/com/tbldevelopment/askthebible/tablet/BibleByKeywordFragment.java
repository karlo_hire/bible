/* This class is used to show verse after which selecting it we finally 
 * goes to BibleDetailFragment class to show Bible*/
package com.tbldevelopment.askthebible.tablet;

import java.util.ArrayList;
import java.util.HashMap;

import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.Utility;
import com.tbldevelopment.askthebible.database.ApplicationAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class BibleByKeywordFragment extends Fragment {

	// Activity Context
	private Context mContext;

	// Array list to store bibles data
	private ArrayList<HashMap<String, String>> listOfBibles;
	// Custom adapter class
	private ApplicationAdapter adapter;

	private HashMap<String, String> mapkeys;
	// Check data loaded or not
	private boolean isLoaded;
	// Progress bar to show porcessing
	private LinearLayout layoutProgress;

	private ListView lv;
	private boolean isKjv=false;
	private View mView;
	String[] web_bibles_title =null;
	String[] kjv_bibles_title=null;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.activity_bible_detail, container,
				false);
		mContext = getActivity();
		isKjv = Utility.getSharedPreferences2(mContext, Constant.BIBLEVERSION[1]);
		final String title = getArguments().getString("title") + ">"
				+ getArguments().getString("bible");
		final String titleBible=getArguments().getString("bible");
		getActivity().getActionBar().setTitle(title);
		// title = "Kings James Version " + title;
        String[] kjvKeyswords = getResources().getStringArray(
				R.array.bible_kjv_keywords);
        String[] webKeyswords = getResources().getStringArray(
				R.array.bible_web_keywords );
		//set title according to bible version web/kjv
        if(isKjv)
        {
		kjv_bibles_title = getResources().getStringArray(
				R.array.bible_kjv_book_title);
        }
        else
        {
		web_bibles_title = getResources().getStringArray(
				R.array.bible_web_book_title);
        }
		//int i = 0;
		if (mapkeys == null) {
			mapkeys = new HashMap<String, String>();
			if(isKjv){
				for (int j = 0; j < kjvKeyswords.length; j++) {
					String key = kjvKeyswords[j];
					mapkeys.put(key, kjv_bibles_title[j]);
				}
			}
			else{
				for (int j = 0; j < webKeyswords.length; j++) {
					String key = webKeyswords[j];
					mapkeys.put(key, web_bibles_title[j]);
			}
			}
		}
		//if(layoutProgress==null)
			layoutProgress = (LinearLayout) mView.findViewById(R.id.layoutProgress);
			lv = (ListView) mView.findViewById(R.id.listViewBibles);

		
//		if(listOfBibles==null)
		listOfBibles = new ArrayList<HashMap<String, String>>();

		//if(adapter==null){
		adapter = new ApplicationAdapter(mContext, -1,
				R.layout.bible_cell_layout, listOfBibles);
		adapter.displayArrow(true);
		
		//if(lv==null)
		
		
		try {
			lv.setAdapter(adapter);
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
//				lv.setVisibility(View.GONE);
				HashMap<String, String> map = listOfBibles.get(arg2);
				String chapter = map.get("chapte_name");
				String section = map.get("section_id");
				Intent in = new Intent(Constant.BROADCAST);
				in.putExtra("sectionid", section);
				in.putExtra("section_key", map.get("section_key"));
				in.putExtra("bible",chapter);
				in.putExtra("title", chapter);
				in.putExtra("book", title);
				in.putExtra("requestFor", Constant.BIBLE_KEYWORD);
				in.putExtra("PANE", Constant.BIBLEDETAIL);
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(in);
				// This broadcast is sent to MainActivity which then starts BibleDetailFragment.
			}
		});
		// justifiedTextView = new JustifiedTextView(mContext);
		System.out.println("Bible couynt "+listOfBibles.size());
		if (listOfBibles.size()==0) {
		String key = getArguments().getString("key");
		System.out.println("this is keys:"+key);

//		new SearchContentOfTopic().execute(key);
	} 
	
		
		return mView;
	}

	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
			String key = getArguments().getString("key");
			new SearchContentOfTopic().execute(key);
		
//		if (listOfBibles.size()>0) 
//		 {
//			getActivity().runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					// TODO Auto-generated method stub
//					layoutProgress.setVisibility(View.GONE);
//					lv.setVisibility(View.VISIBLE);
//					adapter.notifyDataSetChanged();
//				}
//			});
				
		//	}

	}

	public class SearchContentOfTopic extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			layoutProgress.setVisibility(View.VISIBLE);
			lv.setVisibility(View.GONE);
			listOfBibles.clear();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			// keys exa. {Rom.8.12,Gen.4.2}
			String key = params[0];
			if (key == null)
				return null;
			String[] keys = key.split(",");// spliting by the comma
			ArrayList<HashMap<String, String>> listSearch = new ArrayList<HashMap<String, String>>();
			if (keys.length > 0) {
				for (int i = 0; i < keys.length; i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					if (keys[i].contains("-")) {// check if contain exam
												// {Rom.8.12-Rom.8.16}
						String[] keys2 = keys[i].split("-");
						String[] keysection1 = keys2[0].split("\\.");
						String[] keySection = keys2[1].split("\\.");
						String section = null;
						String section_key = null;

						int from = Integer.parseInt(keysection1[2].replace(
								"\\", ""));
						int to = Integer.parseInt(keySection[2].replace("\\",
								""));
//						for (int n = from; n <= to; n++) {
							HashMap<String, String> mapsection = new HashMap<String, String>();
							mapsection.put("book", keysection1[0]);
							if (section == null) {
								section = "{" + keySection[1];
							}
							if (section_key== null){
								section_key="{"+keySection[0]+":"+keySection[1]+":"+from+"}";
							}

							section = section + ":" + from + "}";
							mapsection.put("section", section);
							mapsection.put("section_key", section_key);
							//section = null;
							listSearch.add(mapsection);
							//mapsection = null;
	//					}

					} else {
						String strkey = keys[i];
						String[] keySection = strkey.split("\\.");
						map.put("book", keySection[0]);
						String section_key = null;
						String section = null;
						//for (int n = 1; n < keySection.length; n++) {
							if (section_key == null) {
								section_key = "{"+keySection[0]+":"+ keySection[1]+":"+keySection[2]+"}";
							}
							if(section==null){
								section="{"+keySection[1]+":"+keySection[2]+"}";
							}
//							else {
//								section = section + ":"
//										+ keySection[n].replace("\\", "") + "}";
//							}
//						}
						map.put("section", section);
						map.put("section_key", section_key);

						listSearch.add(map);
					}

				}

			}
			BibleDbAdapter adapter = new BibleDbAdapter(mContext);
			for (int i = 0; i < listSearch.size(); i++) {
				HashMap<String, String> map = listSearch.get(i);
				HashMap<String, String> dataMap = adapter.getBibleFromChapter(
						mapkeys.get(map.get("book")), map.get("section"));
				if (dataMap != null)
					dataMap.put("section_key", map.get("section_key"));	
					listOfBibles.add(dataMap);
			}
			String result = "Success";
			return result;
		}

		protected void onPostExecute(String result) {
			isLoaded = true;
			layoutProgress.setVisibility(View.GONE);
			lv.setVisibility(View.VISIBLE);
			if (result != null) {
				if (result.contains("Success")) {
					adapter.notifyDataSetChanged();
				}

				 adapter.notifyDataSetChanged();
			}
		}

	}

}
