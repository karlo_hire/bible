/*This is for tablet version this fragment is used to 
 * show topics of alphabetical order which will then go
 * to verses that is on BibleByKeyword class*/
package com.tbldevelopment.askthebible.tablet;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class TopicByAlphaScreenFragment extends Fragment{
	
	private LinearLayout layoutProgress;
	private Context mContext;
	private ApplicationStringAdapter adapterString;
	private ArrayList<String> listOfContents;
	private ListView listView;
	private boolean isLoaded;
	private View mView;
	
	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
		      Bundle savedInstanceState)  {

		mView = inflater.inflate(R.layout.right_fragment_list, container, false);
		mContext = getActivity();
		
		final String title = getArguments().getString("alpha") + " Topics";

		getActivity().getActionBar().setTitle(title);
		// Progress bar layout
		//if(layoutProgress==null)
		layoutProgress = (LinearLayout)mView.findViewById(R.id.layoutProgress);
		//else
			layoutProgress.setVisibility(View.GONE);
		//if(listOfContents==null){
		listOfContents = new ArrayList<String>();
		//}
		//if(adapterString==null)
		adapterString = new ApplicationStringAdapter(mContext, -1,
				R.layout.cell_layout, listOfContents);
		
		//if(listView==null)
		 listView = (ListView) mView.findViewById(R.id.listViewCommon);
		 
		 listView.setAdapter(adapterString);
		 listView.setOnItemClickListener(new OnItemClickListener() {

				@SuppressLint("DefaultLocale")
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					// TODO Auto-generated method stub
						String keyword = listOfContents.get(arg2);
						//keyword = keyword.toLowerCase();
						BibleDbAdapter adapter = new BibleDbAdapter(mContext);
						Intent intent = new Intent(Constant.BROADCAST);
						intent.putExtra("bible", listOfContents.get(arg2));
						String key = adapter.getBiblesFromKeyword(keyword);
						intent.putExtra("key", key);
						intent.putExtra("title", title);
						intent.putExtra("PANE", Constant.KEYDETAIL);
						LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
			
				}
			});
		 
		 if(listOfContents.size()==0)
			{
				isLoaded = true;
				String alpha = getArguments().getString("alpha");
				new GetTopics().execute(alpha);
			}
//		 else
//			{
//				layoutProgress.setVisibility(View.GONE);
//				listView.setVisibility(View.VISIBLE);
//				adapterString.notifyDataSetChanged();
//			}
		 
		 return mView;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(listOfContents.size()>0)
		{
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			adapterString.notifyDataSetChanged();
		}
		
	}

	public class GetTopics extends AsyncTask<String, Void, String> {
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			layoutProgress.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String displayText = "";
			int resourcesOfTopics = R.raw.keywords_and_phrases;
			
			//for (int i = 0; i < resourcesOfTopics.length; i++) {
				try {
					InputStream fileStream = null;
					fileStream = getResources().openRawResource(
							resourcesOfTopics);
					int fileLen = fileStream.available();
					// Read the entire resource into a local byte buffer.
					byte[] fileBuffer = new byte[fileLen];
					fileStream.read(fileBuffer);
					fileStream.close();
					displayText = new String(fileBuffer);
				} 
					catch (IOException e) {
					// exception handling
				}

				String[] tabOfShortString = displayText.split("\n");

				for (String strtitle : tabOfShortString) {
					if(strtitle.startsWith(params[0]))
						listOfContents.add(strtitle);
				}
			//}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			adapterString.notifyDataSetChanged();
		}
	}
}
