package com.tbldevelopment.askthebible.tablet;

import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.Utility;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SetupDetailFragment extends Fragment {

	private Context appContext;
	private LayoutInflater lflater;
	private Typeface mFace;
	private View mView;


	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		appContext = getActivity();

		String tag;
		tag = getArguments().getString("TAG");
		mView = inflater.inflate(R.layout.right_fragment_list_setting,
					container, false);

		boolean isKjv;

		isKjv = Utility.getSharedPreferences2(appContext,
				Constant.BIBLEVERSION[1]);
		if (isKjv) {
			getActivity().getActionBar().setTitle("King James Version");
			
		} else {
			getActivity().getActionBar().setTitle("World English Bible");
		}
//		mFace = Typeface.createFromAsset(getActivity().getAssets(),
//				"fonts/verdana.ttf");
		// bible version
		if (tag.equals("0")) {
			
			lflater = (LayoutInflater) getActivity().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			final LinearLayout ll = (LinearLayout) mView
					.findViewById(R.id.layoutOption);
			for (int i = 0; i < Constant.BIBLEVERSION.length; i++) {
				// System.out.println(textEntries[i]);
				final LinearLayout holder = (LinearLayout) lflater.inflate(
						R.layout.cell_layout, null);
				TextView txtTitle = (TextView) holder
						.findViewById(R.id.txtTitle);
				ImageView img = (ImageView) holder
						.findViewById(R.id.imageView2);
				holder.setTag(i);
				txtTitle.setText(Constant.BIBLEVERSION[i]);
				txtTitle.setTypeface(Utility.getTypeFace(appContext));
				boolean isCheck = Utility.getSharedPreferences2(appContext,
						Constant.BIBLEVERSION[i]);
				if (isCheck) {
					img.setImageResource(R.drawable.check_ic);
				} else {
					img.setVisibility(View.GONE);
				}
				if (i == (Constant.BIBLEVERSION.length - 1)) {
					((LinearLayout) holder.findViewById(R.id.cell_seprator))
							.setVisibility(View.GONE);
				}

				ll.addView(holder);
				holder.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int tag = (Integer) v.getTag();
						if (tag == 0) {
							LinearLayout layout = (LinearLayout) ll
									.getChildAt(0);
							LinearLayout layout1 = (LinearLayout) layout
									.getChildAt(0);
							ImageView img = (ImageView) layout1.getChildAt(1);
							img.setVisibility(View.VISIBLE);
							img.setImageResource(R.drawable.check_ic);
							LinearLayout lay = (LinearLayout) ll.getChildAt(1);
							LinearLayout layout2 = (LinearLayout) lay
									.getChildAt(0);
							ImageView img2 = (ImageView) layout2.getChildAt(1);
							img2.setVisibility(View.GONE);
							Utility.setSharedPreference(appContext, "bible",
									"Kjv_Bible_web");
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[0], true);
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[1], false);
							getActivity().getActionBar().setTitle("World English Bible");
							
						} else {
							LinearLayout layout = (LinearLayout) ll
									.getChildAt(0);
							LinearLayout layout1 = (LinearLayout) layout
									.getChildAt(0);
							ImageView img = (ImageView) layout1.getChildAt(1);
							img.setVisibility(View.GONE);
							LinearLayout lay = (LinearLayout) ll.getChildAt(1);
							LinearLayout layout2 = (LinearLayout) lay
									.getChildAt(0);
							ImageView img2 = (ImageView) layout2.getChildAt(1);
							img2.setVisibility(View.VISIBLE);
							img2.setImageResource(R.drawable.check_ic);
							Utility.setSharedPreference(appContext, "bible",
									"Kjv_Bible");
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[0], false);
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[1], true);
							getActivity().getActionBar().setTitle("King James Version");
						}
					}
				});
			}
		}
		return mView;
	}
}
