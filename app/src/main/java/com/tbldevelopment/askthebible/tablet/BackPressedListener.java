package com.tbldevelopment.askthebible.tablet;


import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.tbldevelopment.askthebible.tablet.BaseBackPressedListener.OnBackPressedListener;

public class BackPressedListener implements OnBackPressedListener {

	 private final FragmentActivity activity;
	
	 public BackPressedListener(FragmentActivity activity)
	{
		this.activity = activity;
	}
	@Override
	public void doBack() {
		// TODO Auto-generated method stub
		activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}

}
