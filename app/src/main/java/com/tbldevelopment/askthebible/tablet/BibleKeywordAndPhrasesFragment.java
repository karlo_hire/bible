package com.tbldevelopment.askthebible.tablet;

import java.util.ArrayList;

import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class BibleKeywordAndPhrasesFragment extends Fragment {

	
	private Context mContext;
	private ArrayList<String> listkeywords;
	private boolean keywordloaded;
	private LinearLayout layoutProgress;
	private ListView lv;
	private View mView;
	private ApplicationStringAdapter adapterString;
	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
		      Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.activity_keyword_phrases, container, false);
		mContext = getActivity();
		ActionBar actionbar = getActivity().getActionBar();
		actionbar.setTitle("Alpha Topic");
		if(listkeywords==null)
		listkeywords = new ArrayList<String>();
		
		((TextView)mView.findViewById(R.id.txtLoading)).setText("Please wait while preparing...");
		lv = (ListView) mView.findViewById(R.id.listViewbiblesKeyword);
		//if(layoutProgress==null)
		layoutProgress = (LinearLayout)mView.findViewById(R.id.layoutProgress);
		//else
			layoutProgress.setVisibility(View.GONE);
		//String Adapter
		adapterString = new ApplicationStringAdapter(mContext, 2, R.layout.cell_layout, listkeywords);
	
		lv.setAdapter(adapterString);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				lv.setVisibility(View.GONE);
				String alpha = listkeywords.get(arg2);
				Intent in = new Intent(Constant.BROADCAST);
				in.putExtra("requestFor", Constant.BIBLE_KEYWORD);
				in.putExtra("alpha", alpha);
				in.putExtra("PANE", Constant.ALPHATOPIC);
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(in);
			}
		});
		
		if(listkeywords.size()==0){
			layoutProgress.setVisibility(View.VISIBLE);
			Thread th = new Thread(loadingKeyword);
			th.start();
		}
//		else
//		{
//			lv.setVisibility(View.VISIBLE);
//			layoutProgress.setVisibility(View.GONE);
//			adapterString.notifyDataSetChanged();
//		}
		
		return mView;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}
	
	
	@Override
	public  void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(listkeywords.size()>0)
		{
			lv.setVisibility(View.VISIBLE);
			layoutProgress.setVisibility(View.GONE);
			adapterString.notifyDataSetChanged();
		}
	}
	
	private Runnable loadingKeyword = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			String[] keyswords  = getResources().getStringArray(R.array.alpha_topic);
			for (String key : keyswords) {
				listkeywords.add(key);
			}
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					layoutProgress.setVisibility(View.GONE);
					keywordloaded = true;
					lv.setVisibility(View.VISIBLE);
					adapterString.notifyDataSetChanged();
				}
			});
		}
	};
	
	
}
