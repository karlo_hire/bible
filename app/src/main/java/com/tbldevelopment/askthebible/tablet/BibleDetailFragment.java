/* This class is used to show the detail bible when we select any verse.
 * Here we come from three ways:
 * 1. Through directly selecting topics of Health and your body, god and spirituality,Feelings and emotions etc.
 * 2. Through selecting keywords and phrases from left fragment then topic then verse then here at bible
 * 3. Through selecting "Bible" from left fragment and then topic, then verse and then finally bible   */
package com.tbldevelopment.askthebible.tablet;

import java.util.ArrayList;
import java.util.HashMap;

import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.database.ApplicationAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

public class BibleDetailFragment extends Fragment {

	private Context mContext;
	private LayoutInflater inflater;
	private LinearLayout ll;
	static float density;
	static Point size;
	private ArrayList<HashMap<String, String>> listOfBibles;
	private ApplicationAdapter adapter;
	private HashMap<String, String> mapkeys;
	private int position = -1;
	private int requestFor;
	private String sectionid = null;
	ProgressDialog mProgressDialog;
	private ListView lv;
	private LinearLayout layoutProgress;
	private View mView;
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.activity_bible_detail, container,
				false);
		
		mContext = getActivity();
		String actionBarTitle="";
		requestFor = getArguments().getInt("requestFor", Constant.BIBLE_KEYWORD);

		/**--This much code is used to set title of action bar as the name of the bible book----- **/
		if(requestFor!=Constant.BIBLE){
	    String sectionKey="";
		sectionKey=getArguments().getString("section_key");
		String[] keySplit = sectionKey.split("\\:");
		String keySplitted =	keySplit[0].replaceAll("\\{", " ").trim();
		BibleDbAdapter titleAdapter = new BibleDbAdapter(mContext);
		actionBarTitle=titleAdapter.getBiblesTitlesFromKeyword(keySplitted);
		}
		/***********************--------------------------------------------********************/
		if (requestFor == Constant.BIBLE_KEYWORD) {
			BibleDbAdapter adp = new BibleDbAdapter(mContext);
			String topic = getArguments().getString("bible");
			sectionid = getArguments().getString("sectionid");
			position = adp.getIndexOFChapter(topic, sectionid);
			getActivity().getActionBar().setTitle(getArguments().getString("book")+" >"+actionBarTitle);
		} else{
			final String bookRefrence = getArguments().getString("title");
			String title = null;
			title = "Bible >" + bookRefrence;
			getActivity().getActionBar().setTitle(title);
		}
		
		// if (mapkeys == null)
		mapkeys = new HashMap<String, String>();

			listOfBibles = new ArrayList<HashMap<String, String>>();

			layoutProgress = (LinearLayout) mView
					.findViewById(R.id.layoutProgress);

		
		lv = (ListView) mView.findViewById(R.id.listViewBibles);

		adapter = new ApplicationAdapter(mContext, position,
				R.layout.bible_cell_layout, listOfBibles);
		
		try {
			lv.setAdapter(adapter);
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		// justifiedTextView = new JustifiedTextView(mContext);
		return mView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(listOfBibles.size()>0){
			layoutProgress.setVisibility(View.VISIBLE);
			lv.setVisibility(View.VISIBLE);
		}else{
		String topic = getArguments().getString("bible");
		Log.e("Bible key at detail", topic);
		String chapter = getArguments().getString("chapter");
		Log.e("Bible key at detail"," "+chapter);
		new LoadContetOfTopic().execute(topic,chapter);
		}
//		if (listOfBibles.size() > 0) {
//			lv.setVisibility(View.VISIBLE);
//			layoutProgress.setVisibility(View.GONE);
////			adapter.notifyDataSetChanged();
//		}
//		getActivity().runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				layoutProgress.setVisibility(View.GONE);
//				lv.setVisibility(View.VISIBLE);
//				adapter.notifyDataSetChanged();
//			}
//		});
//
//		if (requestFor == Constant.BIBLE) {
//			String key = getArguments().getString("key");
//			new SearchContentOfTopic().execute(key);
//		} else if (requestFor == Constant.BIBLE_KEYWORD) {
//			String key = getArguments().getString("key");
//			new SearchContentOfTopic().execute(key);
//		}
		
		
		
	}

	public class SearchContentOfTopic extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// mProgressDialog = ProgressDialog.show(mContext,
			// "Loading Content",
			// "Searching...");
			// mProgressDialog.setCanceledOnTouchOutside(false);
			layoutProgress.setVisibility(View.VISIBLE);
			lv.setVisibility(View.GONE);
			listOfBibles.clear();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			// keys exa. {Rom.8.12,Gen.4.2}
			String key = params[0];
			if (key == null)
				return null;
			String[] keys = key.split(",");// spliting by the comma
			ArrayList<HashMap<String, String>> listSearch = new ArrayList<HashMap<String, String>>();
			if (keys.length > 0) {
				for (int i = 0; i < keys.length; i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					if (keys[i].contains("-")) {// check if contain exam
												// {Rom.8.12-Rom.8.16}
						String[] keys2 = keys[i].split("-");
						String[] keysection1 = keys2[0].split("\\.");
						String[] keySection = keys2[1].split("\\.");
						String section = null;

						int from = Integer.parseInt(keysection1[2].replace(
								"\\", ""));
						int to = Integer.parseInt(keySection[2].replace("\\",
								""));
						for (int n = from; n <= to; n++) {
							HashMap<String, String> mapsection = new HashMap<String, String>();
							mapsection.put("book", keysection1[0]);
							if (section == null) {
								section = "{" + keySection[1];
							}

							section = section + ":" + n + "}";
							mapsection.put("section", section);
							section = null;
							listSearch.add(mapsection);
							mapsection = null;
						}

					} else {
						String strkey = keys[i];
						String[] keySection = strkey.split("\\.");
						map.put("book", keySection[0]);
						String section = null;
						for (int n = 1; n < keySection.length; n++) {
							if (section == null) {
								section = "{" + keySection[n].replace("\\", "");
							} else {
								section = section + ":"
										+ keySection[n].replace("\\", "") + "}";
							}
						}
						map.put("section", section);

						listSearch.add(map);
					}

				}

			}
			BibleDbAdapter adapter = new BibleDbAdapter(mContext);
			for (int i = 0; i < listSearch.size(); i++) {
				HashMap<String, String> map = listSearch.get(i);
				HashMap<String, String> dataMap = adapter.getBibleFromChapter(
						mapkeys.get(map.get("book")), map.get("section"));
				if (dataMap != null)
					listOfBibles.add(dataMap);
			}
			String result = "Success";
			return result;
		}

		protected void onPostExecute(String result) {
			layoutProgress.setVisibility(View.GONE);
			lv.setVisibility(View.VISIBLE);
			if (result != null) {
				if (result.contains("Success")) {

					adapter.notifyDataSetChanged();
				}
//				adapter.notifyDataSetChanged();
			}
		}

	}

	public class LoadContetOfTopic extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = ProgressDialog.show(mContext, "Loading Content",
					"Please wait...");
			mProgressDialog.setCanceledOnTouchOutside(false);
//			layoutProgress.setVisibility(View.VISIBLE);
			lv.setVisibility(View.GONE);
			listOfBibles.clear();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = null;
			BibleDbAdapter adapter = new BibleDbAdapter(mContext);
			if (requestFor == Constant.BIBLE_KEYWORD) {
				listOfBibles.addAll(adapter.getBiblesWithKvj(params[0]));
			} else {
				listOfBibles.addAll(adapter.getBiblesWithChapter(params[1],
						params[0]));
			}
			int i = 0;
			for (HashMap<String, String> dataMap : listOfBibles) {
				if (Integer.parseInt(dataMap.get("_id")) == position) {
					position = i;
					Log.i("match pos", "" + position);
				} else if (Integer.parseInt(dataMap.get("_id")) == position + 1) {
					position = i;
					Log.i("match pos", "" + position);
				}
				i = i + 1;
			}
			if (listOfBibles != null) {
				if (listOfBibles.size() > 0) {
					result = "Success";
				} else {
					result = "No bible for the topic.";
				}
			}
			return result;
		}

		protected void onPostExecute(String result) {
			layoutProgress.setVisibility(View.GONE);
			mProgressDialog.cancel();
			lv.setVisibility(View.VISIBLE);
			if (result != null) {
				if (result.contains("Success")) {
					if (requestFor == Constant.BIBLE_KEYWORD) {
						if (position > 0)
						lv.setSelection(position);
						adapter.setPosition(position);
						adapter.notifyDataSetChanged();
					}
//					if (requestFor == Constant.BIBLE) {
//						if (position > 0)
//							lv.setSelection(position);
//						adapter.setPosition(position);
//						adapter.notifyDataSetChanged();
//					}
					adapter.notifyDataSetChanged();
				}
				 adapter.notifyDataSetChanged();
			}
		}
	}

	private Runnable loadContentTopic = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			// String xml = "";
			// InputStream stream = getResources().openRawResource(
			// R.raw.chronicles1);
			// try {
			// byte[] buffer = new byte[stream.available()];
			// stream.read(buffer);
			// stream.close();
			// xml = new String(buffer, "UTF-8");
			//
			// // you just need to specify the charsetName
			// } catch (IOException e) {
			// // Error handling
			// }
			//
			// runOnUiThread(new Runnable() {
			//
			// @Override
			// public void run() {
			// // TODO Auto-generated method stub
			//
			// String[] bibles = xml.split("\n");
			//
			// justifiedTextView.setText(xml);
			// //txtViewVerse.setText(xml.trim());
			// //TextJustification.justify(txtViewVerse, size.x);
			// xml = null;
			//
			// }
			// });
			// final String[] bibles = xml.split("\n");
			//
			// for (int i = 0; i < bibles.length; i++) {
			// final String value = bibles[i];
			// final int length = i;
			//
			// runOnUiThread(new Runnable() {
			// @Override
			// public void run() {
			// // TODO Auto-generated method stub
			// final LinearLayout holder = (LinearLayout) inflater
			// .inflate(R.layout.bible_cell_layout, null);
			// TextView txtTitle = (TextView) holder
			// .findViewById(R.id.txtBible);
			// txtTitle.setVisibility(View.GONE);
			// JustifiedTextView justifiedText = new JustifiedTextView(
			// mContext);
			// // if(length==0){
			// // txtTitle.setGravity(Gravity.CENTER_HORIZONTAL);
			// //
			// // }
			// holder.setTag(value);
			// justifiedText.setText(value);
			// // justifiedText.setTypeface(Utility.getTypeFace(mContext));
			// if (length == (bibles.length - 1)) {
			// ((LinearLayout) holder
			// .findViewById(R.id.cell_seprator))
			// .setVisibility(View.GONE);
			// }
			// holder.addView(justifiedText);
			// ll.addView(holder);
			// }
			// });
			// }
			//
			// runOnUiThread(new Runnable() {
			//
			// @Override
			// public void run() {
			// // TODO Auto-generated method stub
			//
			// }
			// });
		}
	};
}
