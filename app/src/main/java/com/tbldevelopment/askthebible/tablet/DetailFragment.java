package com.tbldevelopment.askthebible.tablet;

import java.util.ArrayList;

import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.Utility;
import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;

import android.app.Fragment;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class DetailFragment extends Fragment {

	private Context mContext;
	private LayoutInflater lflater;
	private ProgressBar mProgressDialog;
	private String displayText = "";
	private Context appContext;
	private Typeface mFace;
	private boolean isContentLoad;
	private ArrayList<String> listOfContents;
	private ApplicationStringAdapter adapterString;
	private LinearLayout layoutProgress;
	int requestFor;
	private ListView listView;
	private View mView;
	private LayoutInflater layflater;
	private ViewGroup contain;
	String[] textEntries;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mContext = getActivity();
		requestFor = getArguments().getInt("requestFor");
//		mFace = Typeface.createFromAsset(mContext.getAssets(),
//				"fonts/Aver Bold.ttf");
		mView = inflater.inflate(R.layout.right_fragment_list_setting,
				container, false);

		return mView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!isContentLoad) {
			isContentLoad = true;
			loadContent();
		}
	}

	@SuppressLint("NewApi")
	private void loadContent() {
		if (requestFor != Constant.SETUP && requestFor != Constant.BIBLE
				&& requestFor != Constant.BIBLE_KEYWORD_PHRASES) {

		} else if (requestFor == Constant.SETUP) {
			lflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			LinearLayout ll = (LinearLayout) getActivity().findViewById(
					R.id.layoutOption);
			for (int i = 0; i < Constant.SETUPNINFO.length; i++) {
				// System.out.println(textEntries[i]);
				final LinearLayout holder = (LinearLayout) lflater.inflate(
						R.layout.cell_layout, null);
				TextView txtTitle = (TextView) holder
						.findViewById(R.id.txtTitle);
				holder.setTag(i);
				txtTitle.setText(Constant.SETUPNINFO[i]);
				txtTitle.setTypeface(Utility.getTypeFace(appContext));
				if (i == (Constant.SETUPNINFO.length - 1)) {
					((LinearLayout) holder.findViewById(R.id.cell_seprator))
							.setVisibility(View.GONE);
				}

				ll.addView(holder);
				holder.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {

						int index = (Integer) v.getTag();
						// Post Review redirect to below url
						if (v.getTag().toString().equals("1") && !v.getTag().toString().equals("0")) {
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setData(Uri
									.parse("market://details?id=com.PandoraTV"));

							if (MyStartActivity(intent) == false) {
								// Market (Google play) app seems not installed,
								// let's try to open a webbrowser
								intent.setData(Uri
										.parse("https://play.google.com/store/apps/details?id=com.PandoraTV"));

							}
							startActivity(intent);
						}
						// feedback to developer redirect to below url
						if (v.getTag().toString().equals("3")) {
							Intent email = new Intent(Intent.ACTION_SEND);
							email.putExtra(
									Intent.EXTRA_EMAIL,
									new String[] { "feedback@askthebibleapp.com" });
							email.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
							email.setType("message/rfc822");
							startActivity(Intent.createChooser(email,
									"Choose an Email client :"));
						}
						if (v.getTag().toString().equals("2")) {
//							Intent intent = new Intent(mContext,
//									WebViewActivity.class);
//							startActivity(intent);

						}

						if (v.getTag().toString().equals("4")) {
							Intent intent = new Intent("ClickLoadPage");
							intent.putExtra("TAG", String.valueOf(index));
							intent.putExtra("requestFor", Constant.ABOUTUS);
							intent.putExtra("PANE", Constant.ABOUTUS);
							LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
									intent);
						}
						if (v.getTag().toString().equals("0")) {
							Intent intent = new Intent("ClickLoadPage");
							intent.putExtra("TAG", String.valueOf(index));
							intent.putExtra("requestFor", Constant.SETUPINFO);
							intent.putExtra("PANE", Constant.SETUPINFO);
							LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
									intent);
						}

					}
				});

			}

			LinearLayout layoutMain = (LinearLayout) getActivity()
					.findViewById(R.id.layoutMainOption);
			layoutMain.setBackgroundResource(R.drawable.corner_style);
		}
	}

	private boolean MyStartActivity(Intent aIntent) {
		try {
			startActivity(aIntent);
			return true;
		} catch (ActivityNotFoundException e) {
			return false;
		}
	}
}
