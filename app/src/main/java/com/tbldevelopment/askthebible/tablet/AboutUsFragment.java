package com.tbldevelopment.askthebible.tablet;

import com.tbldevelopment.askthebible.R;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AboutUsFragment extends Fragment {
	private Context appContext;
	private LayoutInflater lflater;
	private Typeface mFace;
	private View mView,mainview;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		appContext = getActivity();
		String tag;
		tag = getArguments().getString("TAG");
		mView = inflater.inflate(R.layout.right_fragment_aboutus,
					container, false);
		getActivity().getActionBar().setTitle("About Us");
	
		
		return mView;
	}
}
