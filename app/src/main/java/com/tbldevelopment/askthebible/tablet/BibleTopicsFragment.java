package com.tbldevelopment.askthebible.tablet;

import java.util.ArrayList;

import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.Utility;
import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class BibleTopicsFragment extends Fragment {
	private Context appContext;
	private String xml = "";
	private ProgressDialog mProgressDialog;
	private ApplicationStringAdapter adapterString;

	private ArrayList<String> listoftopics;
	private LinearLayout layoutProgress;
	private ListView listView;
	private View mView;
	private boolean isKjv;
	private String[] bibles_title;
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater
				.inflate(R.layout.activity_bible_list, container, false);

		appContext = getActivity();
		// ((MainActivity)getActivity()).setOnBackPressedListener(new BackPressedListener(appContext));

		isKjv = Utility.getSharedPreferences2(appContext,
				Constant.BIBLEVERSION[1]);
		if (isKjv) {
			getActivity().getActionBar().setTitle("King James Version");
			bibles_title = getResources().getStringArray(
					R.array.bible_kjv_book_title);
		} else {
			getActivity().getActionBar().setTitle("World English Bible");
			bibles_title = getResources().getStringArray(
					R.array.bible_web_book_title);
		}
		if(listoftopics==null)
		listoftopics = new ArrayList<String>();
		
		
		//if(listView==null)
		listView = (ListView) mView.findViewById(R.id.listViewBibletopic);
		layoutProgress = (LinearLayout) mView.findViewById(R.id.layoutProgress);
		//if(adapterString==null)
		adapterString = new ApplicationStringAdapter(appContext, -1,
				R.layout.cell_layout, listoftopics);

		listView.setAdapter(adapterString);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int tag,
					long arg3) {
				// TODO Auto-generated method stub
				listView.setVisibility(View.GONE);
				String topic = bibles_title[tag];
				final String value = listoftopics.get(tag);
				Intent in = new Intent(Constant.BROADCAST);
				in.putExtra("title", value);
				in.putExtra("bible", topic);
				in.putExtra("requestFor", Constant.BIBLE_SECTION);
				in.putExtra("PANE", Constant.BIBLESECTION);
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						in);

			}
		});
		// mProgressDialog = ProgressDialog.show(appContext, "Please Wait...",
		// "Loading Topics");
		if (listoftopics.size() == 0) {
			layoutProgress.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
			Thread th = new Thread(loadTopics);
			th.start();
		}
		else
		{
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);

		}

		return mView;
	}
	
//	
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	  super.onSaveInstanceState(savedInstanceState);
	 
	}


	public void onResume() {
		super.onResume();
		
		if (listoftopics.size() > 0) {
			listView.setVisibility(View.VISIBLE);
			layoutProgress.setVisibility(View.GONE);
			adapterString.notifyDataSetChanged();

		}
	}

	Runnable loadTopics = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
		
			createTopicList();
		}
	};

	
	
	private void createTopicList() {
		String[] topics = null;
		if (isKjv)
			topics = getResources().getStringArray(R.array.bible_list);
		else
			topics = getResources().getStringArray(R.array.bible_list_web);

		for (String key : topics) {
			listoftopics.add(key);
		}

		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				layoutProgress.setVisibility(View.GONE);
				listView.setVisibility(View.VISIBLE);
				adapterString.notifyDataSetChanged();
			}
		});

	}

}
