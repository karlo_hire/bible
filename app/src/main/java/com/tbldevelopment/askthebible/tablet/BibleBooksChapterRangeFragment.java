package com.tbldevelopment.askthebible.tablet;

import java.util.ArrayList;
import com.tbldevelopment.askthebible.Constant;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.Utility;
import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class BibleBooksChapterRangeFragment extends Fragment {
	private ApplicationStringAdapter adapterString;
	private Context mContext;
	private ArrayList<String> listOfNumChapter;
	private ListView listView;
	private boolean isDataloaded;
	private String strChapterTitle;
	private View mView;
	private LinearLayout layoutProgress;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		      Bundle savedInstanceState) {
		// TODO Auto-generated method stub
	
		mView = inflater.inflate(R.layout.activity_bible_list, container, false);
		mContext =  getActivity();
		
		// Get title of book
		strChapterTitle = getArguments().getString("bible");
				
		final String bookRefrence = getArguments().getString("title");
		boolean isKjv = Utility.getSharedPreferences2(mContext, Constant.BIBLEVERSION[1]);
		String title = null;
		if (isKjv) {
			title = "Bible >"+bookRefrence;
		}else
		{
			title = "Bible >"+bookRefrence;
		}
		getActivity().getActionBar().setTitle(title);
		
		
		// Progress Layout
		//if(layoutProgress==null)
		layoutProgress = (LinearLayout)mView.findViewById(R.id.layoutProgress);
		
		// Array list to store all chapterid
		if(listOfNumChapter==null)
		listOfNumChapter = new ArrayList<String>();
		
		//List view
		//if(listView==null)
		listView = (ListView)mView.findViewById(R.id.listViewBibletopic);
		
		//String Adapter
		//if(adapterString==null)
		adapterString = new ApplicationStringAdapter(mContext, 2, R.layout.cell_layout, listOfNumChapter);
		
		// Set adapter to list view
		
		listView.setAdapter(adapterString);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				String chapter = listOfNumChapter.get(arg2);
				Intent in = new Intent(Constant.BROADCAST);
				in.putExtra("title", getArguments().getString("title"));
				in.putExtra("bible", strChapterTitle);
				in.putExtra("requestFor", Constant.BIBLE);
				in.putExtra("PANE", Constant.BIBLEDETAIL);
				in.putExtra("chapter", chapter);
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(in);
			}
		});
	
		if(listOfNumChapter.size()==0)
		{
			isDataloaded = true;
			new LoadContetOfTopic().execute("");
		}
//		else
//		{
//			layoutProgress.setVisibility(View.GONE);
//			listView.setVisibility(View.VISIBLE);
//			adapterString.notifyDataSetChanged();
//		}
		
		return mView;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(listOfNumChapter.size()>0)
		{
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			adapterString.notifyDataSetChanged();
		}else{
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}
		
	}
	
	public class LoadContetOfTopic extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			layoutProgress.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
			listOfNumChapter.clear();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = null;
			
			BibleDbAdapter adapter = new BibleDbAdapter(mContext);
			listOfNumChapter.addAll(adapter.getNumberofchapter(strChapterTitle));
		
			if (listOfNumChapter != null) {
				if (listOfNumChapter.size() > 0) {
					result = "Success";
				} else {
					result = "No bible for the topic.";
				}
			}
			return result;
		}

		protected void onPostExecute(String result) {
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			if (result != null) {
				if (result.contains("Success")) {
					adapterString.notifyDataSetChanged();
				}
				// adapter.notifyDataSetChanged();
			}
		}
	}
}
