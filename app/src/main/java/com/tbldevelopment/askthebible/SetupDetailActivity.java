package com.tbldevelopment.askthebible;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SetupDetailActivity extends Activity {

	private Context appContext;
	private LayoutInflater lflater;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String tag;
		Bundle bundle = getIntent().getExtras();
		tag = bundle.getString("TAG");
		if (tag.equalsIgnoreCase("4")) {
			setContentView(R.layout.right_fragment_aboutus);
		}if (tag.equalsIgnoreCase("5")) {
			setContentView(R.layout.right_fragment_scripture);
		}
		else {
			setContentView(R.layout.right_fragment_list_setting);

		}
		appContext = this;
		
        // set action bar according to preference set.
		boolean isKjv;
		ActionBar actionBar = getActionBar();
		isKjv = Utility.getSharedPreferences2(appContext,
				Constant.BIBLEVERSION[1]);
		if (isKjv) {
			actionBar.setTitle("King James Version");
		} else {
			actionBar.setTitle("World English Bible");

		}

		
		// bible version
		if (tag.equals("0")) {
			lflater = (LayoutInflater) getApplicationContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final LinearLayout ll = (LinearLayout) findViewById(R.id.layoutOption);
			for (int i = 0; i < Constant.BIBLEVERSION.length; i++) {
				// System.out.println(textEntries[i]);
				final LinearLayout holder = (LinearLayout) lflater.inflate(
						R.layout.cell_layout, null);
				TextView txtTitle = (TextView) holder
						.findViewById(R.id.txtTitle);
				ImageView img = (ImageView) holder
						.findViewById(R.id.imageView2);
				holder.setTag(i);
				txtTitle.setText(Constant.BIBLEVERSION[i]);
				boolean isCheck = Utility.getSharedPreferences2(appContext,
						Constant.BIBLEVERSION[i]);
				if (isCheck) {
					img.setImageResource(R.drawable.check_ic);
				} else {
					img.setVisibility(View.GONE);
				}
				txtTitle.setTypeface(Utility.getTypeFace(appContext));
				if (i == (Constant.BIBLEVERSION.length - 1)) {
					((LinearLayout) holder.findViewById(R.id.cell_seprator))
							.setVisibility(View.GONE);
				}
				// Set two options in listview as "World English Bible" and "King James Version"
				ll.addView(holder);
				holder.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int tag = (Integer) v.getTag();
						if (tag == 0) {
							LinearLayout layout = (LinearLayout) ll
									.getChildAt(0);
							LinearLayout layout1 = (LinearLayout) layout
									.getChildAt(0);
							ImageView img = (ImageView) layout1.getChildAt(1);
							img.setVisibility(View.VISIBLE);
							img.setImageResource(R.drawable.check_ic);
							LinearLayout lay = (LinearLayout) ll.getChildAt(1);
							LinearLayout layout2 = (LinearLayout) lay
									.getChildAt(0);
							ImageView img2 = (ImageView) layout2.getChildAt(1);
							img2.setVisibility(View.GONE);
							Utility.setSharedPreference(appContext, "bible",
									"Kjv_Bible_web");
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[0], true);
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[1], false);
						} else {
							LinearLayout layout = (LinearLayout) ll
									.getChildAt(0);
							LinearLayout layout1 = (LinearLayout) layout
									.getChildAt(0);
							ImageView img = (ImageView) layout1.getChildAt(1);
							img.setVisibility(View.GONE);
							LinearLayout lay = (LinearLayout) ll.getChildAt(1);
							LinearLayout layout2 = (LinearLayout) lay
									.getChildAt(0);
							ImageView img2 = (ImageView) layout2.getChildAt(1);
							img2.setVisibility(View.VISIBLE);
							img2.setImageResource(R.drawable.check_ic);
							Utility.setSharedPreference(appContext, "bible",
									"Kjv_Bible");
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[0], false);
							Utility.setSharedPreferenceBoolean(appContext,
									Constant.BIBLEVERSION[1], true);
						}
					}
				});
			}
		}

	}
}
