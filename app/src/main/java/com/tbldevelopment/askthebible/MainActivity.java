package com.tbldevelopment.askthebible;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tbldevelopment.askthebible.database.BibleDbAdapter;
import com.tbldevelopment.askthebible.tablet.AboutUsFragment;
import com.tbldevelopment.askthebible.tablet.BaseBackPressedListener.OnBackPressedListener;
import com.tbldevelopment.askthebible.tablet.BibleBooksChapterRangeFragment;
import com.tbldevelopment.askthebible.tablet.BibleByKeywordFragment;
import com.tbldevelopment.askthebible.tablet.BibleDetailFragment;
import com.tbldevelopment.askthebible.tablet.BibleKeywordAndPhrasesFragment;
import com.tbldevelopment.askthebible.tablet.BibleTopicsFragment;
import com.tbldevelopment.askthebible.tablet.ScriptureFragment;
import com.tbldevelopment.askthebible.tablet.SetupDetailFragment;
import com.tbldevelopment.askthebible.tablet.TopicByAlphaScreenFragment;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.LinearLayout;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends Activity {
	int requestFor = 2;
	String pane;
	private ProgressDialog dialog;
	private String[] bibles = { "genesis", "exodus", "leviticus", "number",
			"deuteronomy", "joshua", "judges", "ruth", "samuel1", "samuel2",
			"kings1", "kings2", "chronicles1", "chronicles2", "ezra",
			"nehemiah", "esther", "job", "psalms", "proverbs", "ecclesiastes",
			"song", "isaiah", "jeremiah", "lamentations", "ezekiel", "daniel",
			"hosea", "joel", "amos", "obadiah", "jonah", "micah", "nahum",
			"habakkuk", "zeph", "haggai", "zech", "malachi", "tobit", "judith",
			"wisdom", "sirach", "baruch", "azar", "susanna", "bel", "mac1",
			"mac2", "esdras1", "man", "esdras2", "matthew", "mark", "luke",
			"john", "ats", "romans", "cor1", "cor2", "gal", "eph", "philip",
			"col", "thes1", "thes2", "tim1", "tim2", "titus", "philemon",
			"hebrews", "james", "peter1", "peter2", "john1", "john2", "john3",
			"jude", "rev" };
	private String[] keys = { "genesis", "exodus", "leviticus", "number",
			"deuteronomy", "joshua", "judges", "ruth", "samuel1", "samuel2",
			"kings1", "kings2", "chronicles1", "chronicles2", "ezra",
			"nehemiah", "esther", "job", "psalms", "proverbs", "ecclesiastes",
			"song", "isaiah", "jeremiah", "lamentations", "ezekiel", "daniel",
			"hosea", "joel", "amos", "obadiah", "jonah", "micah", "nahum",
			"habakkuk", "zeph", "haggai", "zech", "malachi", "tobit", "judith",
			"wisdom", "sirach", "baruch", "azar", "susanna", "bel", "mac1",
			"mac2", "esdras1", "man", "esdras2", "matthew", "mark", "luke",
			"john", "ats", "romans", "cor1", "cor2", "gal", "eph", "philip",
			"col", "thes1", "thes2", "tim1", "tim2", "titus", "philemon",
			"hebrews", "james", "peter1", "peter2", "john1", "john2", "john3",
			"jude", "rev" };
	private String[] bibles_title = {
			"The First Book of Moses, called Genesis",
			"The Second Book of Moses, called Exodus",
			"The Third Book of Moses, called Leviticus",
			"The Fourth Book of Moses, called Numbers",
			"The Fifth Book of Moses, called Deuteronomy",
			"The Book of Joshua",
			"The Book of Judges",
			"The Book of Ruth",
			"The First Book of Samuel Otherwise Called the First Book of the Kings",
			"The Second Book of Samuel Otherwise Called the Second Book of the Kings",
			"The First Book of the Kings Commonly Called the Third Book of the Kings",
			"The Second Book of the Kings Commonly Called the Fourth Book of the Kings",
			"The First Book of the Chronicles",
			"The Second Book of the Chronicles", "Ezra",
			"The Book of Nehemiah", "The Book of Esther", "The Book of Job",
			"The Book of Psalms", "The Proverbs",
			"Ecclesiastes or, the Preacher", "The Song of Solomon",
			"The Book of the Prophet Isaiah",
			"The Book of the Prophet Jeremiah", "The Lamentations of Jeremiah",
			"The Book of the Prophet Ezekiel", "The Book of Daniel", "Hosea",
			"Joel", "Amos", "Obadiah", "Jonah", "Micah", "Nahum", "Habakkuk",
			"Zephaniah", "Haggai", "Zechariah", "Malachi", "The Book of Tobit",
			"The Book of Judith",
			"The Book of Wisdom or The Wisdom of Solomon",
			"The Wisdom of Jesus the Son of Sirach, or Ecclesiasticus",
			"The Book of Baruch", "The Prayer of Azariah",
			"The History of Susanna [in Daniel]",
			"The Book of Bel and the Dragon [in Daniel]",
			"The First Book of the Maccabees",
			"The Second Book of the Maccabees", "The First Book of Esdras",
			"The Prayer of Manasseh, or, The Prayer of Manasses King of Judah",
			"The Second Book of Esdras", "The Gospel According to St. Matthew",
			"The Gospel According to St. Mark",
			"The Gospel According to St. Luke",
			"The Gospel According to St. John", "The Acts of the Apostles",
			"The Epistle of Paul the Apostle to the Romans ",
			"The First Epistle of Paul the Apostle to the Corinthians",
			"The Second Epistle of Paul the Apostle to the Corinthians",
			"The Epistle of Paul the Apostle to the Galatians",
			"The Epistle of Paul the Apostle to the Ephesians",
			"The Epistle of Paul the Apostle to the Philippians",
			"The Epistle of Paul the Apostle to the Colossians",
			"The First Epistle of Paul to the Thessalonians",
			"The Second Epistle of Paul the Apostle to the Thessalonians",
			"The First Epistle of Paul the Apostle to Timothy",
			"The Second Epistle of Paul the Apostle to Timothy",
			"The Epistle of Paul to Titus", "The Epistle of Paul to Philemon",
			"The Epistle to the Hebrews", "The General Epistle of James",
			"The First Epistle General of Peter",
			"The Second Epistle General of Peter",
			"The First Epistle General of John", "The Second Epistle of John",
			"The Third Epistle of John", "The General Epistle of Jude",
			"The Revelation of St. John the Divine" };
	private int[] bible_noofPage = { 50, 40, 27, 36, 34, 24, 21, 4, 31, 24, 22,
			25, 29, 36, 10, 13, 16, 42, 150, 31, 12, 8, 66, 52, 5, 48, 12, 14,
			3, 9, 1, 4, 7, 3, 3, 3, 2, 14, 4, 14, 16, 19, 51, 5, 1, 1, 1, 16,
			15, 9, 1, 16, 28, 16, 24, 21, 28, 16, 16, 13, 6, 6, 4, 4, 5, 3, 6,
			4, 3, 1, 13, 5, 5, 3, 5, 1, 1, 1, 22 };
	private Context mContext;
	protected OnBackPressedListener onBackPressedListener;
	private Stack<Fragment> mFragmentStack = new Stack<Fragment>();
	private ArrayList<Fragment> fragmentlist = new ArrayList<Fragment>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
		System.out.println("TAB" + tabletSize);
		System.out.print("bible = " + bibles.length + "title = "
				+ bibles_title.length + "pages " + bible_noofPage.length);
        
		if (tabletSize) {
			setContentView(R.layout.activity_main);

			LinearLayout layoutLeftPane = (LinearLayout) findViewById(R.id.layoutLeftPane);
			Resources r = getResources();
			int width = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 320, r.getDisplayMetrics());
			int heifht = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP,
					layoutLeftPane.getLayoutParams().height,
					r.getDisplayMetrics());
			ActionBar bar = getActionBar();
			bar.setTitle("Ask the Bible");
			layoutLeftPane.setLayoutParams(new LinearLayout.LayoutParams(width,
					heifht));
			
			
			System.out.println("1");
		} else {
			setContentView(R.layout.activity_main_small);
			System.out.println("0");
			ActionBar bar = getActionBar();
			bar.setTitle("Ask the Bible");
		}
		mContext = this;
		if (!Utility.getSharedPreferences2(mContext, "database")) {
			checkDatabase();
		}

	}

	public void setOnBackPressedListener(
			OnBackPressedListener onBackPressedListener) {
		this.onBackPressedListener = onBackPressedListener;
	}

	void checkDatabase() {
		ContextWrapper wrapper = new ContextWrapper(mContext);
		String outfilename = wrapper.getDatabasePath("Kjv_Bible.sqlite")
				.getAbsolutePath().toString();
		// wrapper.openOrCreateDatabase("Kjv_Bible.sqlite",
		// ContextWrapper.MODE_ENABLE_WRITE_AHEAD_LOGGING, null);

		BibleDbAdapter db = new BibleDbAdapter(mContext);
		File f = new File(outfilename);

		if (!f.exists()) {
			// db.createDb();
			db.close();
			db = null;

			new LoadBible().execute(outfilename);
		} else {
			db.close();
			new LoadBible().execute(outfilename);
		}

	}

	ProgressDialog pDialog;

	public class LoadBible extends AsyncTask<String, String, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MainActivity.this);
			pDialog.setMessage("Loading bibles. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			// insertKeywords();
			Utility.setSharedPreference(mContext, "bible", "Kjv_Bible_web");
			Utility.setSharedPreferenceBoolean(mContext, "King James Bible",
					false);
			Utility.setSharedPreferenceBoolean(mContext, "World English Bible",
					true);
			
			try {
				InputStream myinput = mContext.getAssets().open(
						"Kjv_Bible.sqlite");
				// BibleDbAdapter db = new BibleDbAdapter(mContext);
				// db.close();
				// db = null;
				OutputStream myoutput = new FileOutputStream(params[0]);
				int lenghtOfFile = myinput.available();
				// transfer byte to inputfile to outputfile
				byte[] buffer = new byte[1024];
				int length;
				long total = 0;
				while ((length = myinput.read(buffer)) > 0) {
					total += length;
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));
					myoutput.write(buffer, 0, length);
				}

				// Close the streams
				myoutput.flush();
				myoutput.close();
				myinput.close();
			} catch (Exception e) {
				e.printStackTrace();
				LoadBible.this.cancel(true);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						checkDatabase();
					}
				});
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... progress) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(progress);
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		protected void onPostExecute(String result) {

			if (pDialog != null)
				pDialog.dismiss();
			Utility.setSharedPreferenceBoolean(mContext, "database", true);
		}
	}

	public class LoadKeyWord extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = ProgressDialog.show(MainActivity.this, "Preparing Bibles",
					"Please wait...");
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(true);
			dialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					LoadKeyWord.this.cancel(true);
				}
			});
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			//insertKeywords();
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (dialog != null)
				dialog.cancel();
		}
	}

	private void insertKeywords() {
		BibleDbAdapter dbAdapter = new BibleDbAdapter(MainActivity.this);
		String jsonString = "";
		InputStream stream = getResources().openRawResource(R.raw.keywords);
		try {
			byte[] buffer = new byte[stream.available()];
			stream.read(buffer);
			stream.close();
			jsonString = new String(buffer);

			JSONObject jsonObject = new JSONObject(jsonString);
			jsonObject = jsonObject.getJSONObject("Workbook");
			JSONArray jArray = jsonObject.getJSONArray("Cell");
			
			HashMap<String, String> map = new HashMap<String, String>();
			for (int i = 0; i < jArray.length(); i++) {
//				JSONArray jsonArray = jArray.getJSONObject(i).getJSONArray(
//						"Cell");
				String keyword = null, chapter = null;
				JSONObject object = jArray.getJSONObject(i);
				
				keyword= object.getString("data");
				chapter= object.getString("text");
//				String keyword = null, chapter = null;
//				for (int j = 0; j < jsonArray.length(); j++) {
//					JSONObject object = jsonArray.getJSONObject(j);
//					object = object.getJSONObject("Data");
//					if (j == 0) {
//						keyword = object.getString("#text");
//					} else {
//						if (chapter == null) {
//							chapter = object.getString("#text");
//						} else {
//							chapter = chapter + "," + object.getString("#text");
//						}
//					}
//				}
				map.put("keyword",keyword);
				System.out.println("This is keyword: "+keyword);
				map.put("chapter_info",chapter);
				System.out.println("This is chapter: "+chapter);
				dbAdapter.insertKeywordRecord(map);
			}
			
			

			// you just need to specify the charsetName
		} catch (IOException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		} catch (JSONException e) {
			System.out.println(e.toString());			 
			e.printStackTrace();
		}

	}

	private void insertBiblesData() {
		BibleDbAdapter dbAdapter = new BibleDbAdapter(MainActivity.this);

//		 for (int k = 0; k < bibles.length; k++) {
//		 String xml = "";
//		 int resId = getResources().getIdentifier(bibles[k], "raw",
//		 getPackageName());
//		 InputStream stream = getResources().openRawResource(resId);
//		 try {
//		 byte[] buffer = new byte[stream.available()];
//		 stream.read(buffer);
//		 stream.close();
//		 xml = new String(buffer, "UTF-8");
//		
//		 // you just need to specify the charsetName
//		 } catch (IOException e) {
//		 // Error handling
//		 }
//		
//		 for (int i = 0; i < bible_noofPage[k]; i++) {
//		 String seperate = "\\{" + (i + 1) + ":";
//		 String[] sections = xml.trim().split(seperate);
//		 for (int j = 1; j < sections.length; j++) {
//		 String replacestr = "" + (j) + "}";
//		 seperate = seperate.replace("\\", "");
//		 String sectonid = seperate + replacestr;
//		 HashMap<String, String> map = new HashMap<String, String>();
//		 map.put("chapte_name", bibles_title[k]);
//		 map.put("section_id", sectonid);
//		 map.put("section", "" + (i + 1));
//		
//		 if ((j + 1) == sections.length) {
//		 String temp = "\\{" + (i + 2) + ":";
//		 String[] lastvalue = sections[j].trim().replace(replacestr, "").split(temp);
//		 if (lastvalue.length > 0) {
//		 map.put("verse", lastvalue[0]);
//		 xml = sections[j].trim().replace(replacestr, "");
//		 } else {
//		 map.put("verse",
//		 sections[j].trim().replace(replacestr, ""));
//		 }
//		
//		 lastvalue = null;
//		 temp = null;
//		 } else {
//		 map.put("verse",
//		 sections[j].trim().replace(replacestr, ""));
//		 }
//		
//		 dbAdapter.insertBibleRecord(map);
//		 }
//		 }
//		 }

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// Local broad cast manager
		LocalBroadcastManager.getInstance(getApplicationContext())
				.registerReceiver(mBraoadCaseMesagner,
						new IntentFilter("ClickLoadPage"));
		// new LoadKeyWord().execute("");

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		LocalBroadcastManager.getInstance(MainActivity.this)
		.unregisterReceiver(mBraoadCaseMesagner);
//
//		RightFragment fragment = (RightFragment) getFragmentManager()
//				.findFragmentById(R.id.right_fragment);
//		if(fragment!=null && fragment.isInLayout()){
//			fragment.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//		}
		
	}
	
	protected void onPause(){
		super.onPause();
	
	}

	public BroadcastReceiver mBraoadCaseMesagner = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Bundle bundle = intent.getExtras();
			requestFor = bundle.getInt("requestFor");
			pane = bundle.getString("PANE");
			onRssItemSelected(requestFor, pane, bundle);
			
		}
	};

	public void onBackPressed() {
		// super.onBackPressed();
		RightFragment fragment = (RightFragment) getFragmentManager()
				.findFragmentById(R.id.right_fragment);
		if(fragment!=null && fragment.isInLayout()){
		android.app.FragmentManager fm = getFragmentManager();
	    if (fm.getBackStackEntryCount() > 0) {
	        Log.i("MainActivity", "popping backstack");
	        fm.popBackStackImmediate();
	        
	    } else {
	        Log.i("MainActivity", "nothing on backstack, calling super");
	        super.onBackPressed();  
	    }
	   }else
	   {
		   super.onBackPressed();    
	   }
		
		
//		mFragmentStack.pop();
//		
//		if (mFragmentStack.size() > 0) {
//			FragmentTransaction ft = getFragmentManager().beginTransaction();
//			Log.d("", mFragmentStack.peek().getClass().getName());
//			ft.replace(R.id.layoutRightPane, mFragmentStack.peek());
//			ft.commit();
//		} else {
//			
//			super.onBackPressed();
//		}
		
	};

	public void onRssItemSelected(int requestFor, String link, Bundle bundle) {
		// TODO Auto-generated method stub
		RightFragment fragment = (RightFragment) getFragmentManager()
				.findFragmentById(R.id.right_fragment);

		if (fragment != null && fragment.isInLayout()) {
						
			if (link.equals("left")) {

				RightFragment newFragment = new RightFragment();
				if (bundle == null)
					bundle = new Bundle();
				bundle.putInt("requestFor", requestFor); // any string to be
				// sent
				newFragment.setArguments(bundle);

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane, newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

				System.out.println("In If-left pane");
				Utility.setSharedPreference(getApplicationContext(), "Pref",requestFor);
				System.out.println(requestFor);
			} else if (link.equals(Constant.KEYPHASES)) {

				BibleKeywordAndPhrasesFragment newFragment = new BibleKeywordAndPhrasesFragment();
				if (bundle == null)
					bundle = new Bundle();
				bundle.putInt("requestFor", requestFor); // any string to be
															// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

			} else if (link.equals("right")) {
				OptionsFragment newFragment = new OptionsFragment();
				if (bundle == null)
					bundle = new Bundle();

				bundle.putInt("requestFor", requestFor); // any string to be
															// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

			} else if (link.equals(Constant.BIBLESECTION)) {
				BibleBooksChapterRangeFragment newFragment = new BibleBooksChapterRangeFragment();
				if (bundle == null)
					bundle = new Bundle();
				bundle.putInt("requestFor", requestFor); // any string to be
				// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

			} else if (link.equals(Constant.ALPHATOPIC)) {
				TopicByAlphaScreenFragment newFragment = new TopicByAlphaScreenFragment();
				if (bundle == null)
					bundle = new Bundle();
				bundle.putInt("requestFor", requestFor); // any string to be
				// sent
				newFragment.setArguments(bundle);

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

			} else if (link.equals(Constant.KEYDETAIL)) {
				BibleByKeywordFragment newFragment = new BibleByKeywordFragment();
				if (bundle == null)
					bundle = new Bundle();

				bundle.putInt("requestFor", requestFor); // any string to be
															// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

			} else if (link.equals(Constant.BIBLEDETAIL)) {
				BibleDetailFragment newFragment = new BibleDetailFragment();
				if (bundle == null)
					bundle = new Bundle();

				bundle.putInt("requestFor", requestFor); // any string to be sent
				newFragment.setArguments(bundle);

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

			} else if (link.equals(Constant.BIBLETOPIC)) {
				BibleTopicsFragment newFragment = new BibleTopicsFragment();
				if (bundle == null)
					bundle = new Bundle();

				bundle.putInt("requestFor", requestFor); // any string to be
															// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();

			} else if (link.equals(Constant.SETUPINFO)) {
				SetupDetailFragment newFragment = new SetupDetailFragment();
				if (bundle == null)
					bundle = new Bundle();
				bundle.putInt("requestFor", requestFor); // any string to be
															// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();
			} else if(link.equals(Constant.ABOUTUS)) {
				AboutUsFragment newFragment = new AboutUsFragment();
				if (bundle == null)
					bundle = new Bundle();
				bundle.putInt("requestFor", requestFor); // any string to be
															// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();
			}
			else if(link.equals(Constant.SCRIPTURE)) {
				ScriptureFragment newFragment = new ScriptureFragment();
				if (bundle == null)
					bundle = new Bundle();
				bundle.putInt("requestFor", requestFor); // any string to be
															// sent
				newFragment.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.replace(R.id.layoutRightPane,
						newFragment);
				transaction.addToBackStack(null);
				transaction.commit();
			}

		} else {
			System.out.println("IN ELSE");
			Intent intent = new Intent(getApplicationContext(),
					DetailActivity.class);
			intent.putExtra("requestFor", requestFor);
			startActivity(intent);

		}
	}
}
