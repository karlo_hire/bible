package com.tbldevelopment.askthebible.database;

import java.util.ArrayList;

import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.Utility;




import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ApplicationStringAdapter extends ArrayAdapter<String> {
	ArrayList<String> applist;
	Context mContext;
	int count = -1;
	LayoutInflater inflater;
	int position;
	public ApplicationStringAdapter(Context context, int resource,
			int textViewResourceId, ArrayList<String> objects) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		applist = objects;
		mContext = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		position = resource;
		
	}

	@Override
	public int getCount() {
		return applist.size();
	}
	
	
	

	public View getView(final int aPosition, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View viewHolder = convertView;
		
		count =getCount()-1;
		if (viewHolder == null) {
			holder = new ViewHolder();
			viewHolder = inflater.inflate(R.layout.cell_layout,
					null);
			holder.imageArrow = (ImageView)viewHolder.findViewById(R.id.imageView2);
			holder.imageArrow.setVisibility(View.VISIBLE);
			holder.txtVerse = (TextView)viewHolder.findViewById(R.id.txtTitle);
			holder.txtVerse.setTextSize(16);
			holder.txtVerse.setTypeface(Utility.getTypeFace(mContext));
			
			viewHolder.setTag(holder);
			
		}else
		{
			holder = (ViewHolder)viewHolder.getTag();
		}
		
		String label = applist.get(aPosition);//getItem(aPosition);
		
		holder.txtVerse.setText(label);
		
		if (count > 0) {
			if (aPosition == 0) {
				viewHolder.setBackgroundResource(R.drawable.top_corner_bg);
			} else if (aPosition == count) {
				viewHolder.setBackgroundResource(R.drawable.bottom_corner_bg);
			} else {
				viewHolder.setBackgroundResource(R.drawable.middle_bg);
			}
		}
		
		return viewHolder;

	}

	class ViewHolder {
		TextView txtVerse;
		LinearLayout layoutBibleCell;
		ImageView imageArrow;
	}
}
