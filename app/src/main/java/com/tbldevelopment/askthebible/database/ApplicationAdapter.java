package com.tbldevelopment.askthebible.database;

import java.util.ArrayList;
import java.util.HashMap;

import com.tbldevelopment.askthebible.JustifiedTextView;
import com.tbldevelopment.askthebible.R;
import com.tbldevelopment.askthebible.Utility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ApplicationAdapter extends ArrayAdapter<HashMap<String, String>> {
	ArrayList<HashMap<String, String>> applist;
	Context mContext;
	int count = -1;
	LayoutInflater inflater;
	int position;
	boolean isArrowShow;
	

	public ApplicationAdapter(Context context, int resource,
			int textViewResourceId, ArrayList<HashMap<String, String>> objects) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		applist = objects;
		mContext = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		position = resource;
		
	}

	@Override
	public int getCount() {
		return applist.size();
	}
	
	public void setPosition(int pos)
	{
		position = pos+1;
		System.out.println("This is in application adapter :"+position);
	}
	public void displayArrow(boolean isdisplay)
	{
		isArrowShow = isdisplay;
	}

	public View getView(final int aPosition, View convertView, ViewGroup parent) {
		ViewHolder holder;
		View viewHolder = convertView;
		
		count = getCount()-1;
		
		if (viewHolder == null) {
			holder = new ViewHolder();
			viewHolder = inflater.inflate(R.layout.bible_cell_layout, null);
			holder.layoutBibleCell = (LinearLayout) viewHolder
					.findViewById(R.id.layoutBibleCell);
			holder.imageArrow = (ImageView)viewHolder.findViewById(R.id.imageView2);
			
		//	holder.textVJustify = new JustifiedTextView(mContext);
			//holder.layoutBibleCell.addView(holder.textVJustify,
					//new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
							//LayoutParams.WRAP_CONTENT));

			holder.txtVerse = (TextView) viewHolder.findViewById(R.id.txtBible);
			holder.txtVerse.setTextSize(16);
			//holder.txtVerse.setVisibility(View.GONE);
			holder.txtVerse.setTypeface(Utility.getTypeFace(mContext));
			viewHolder.setTag(holder);
		

		} else {
			holder = (ViewHolder) viewHolder.getTag();
		}
		if(isArrowShow)
			holder.imageArrow.setVisibility(View.VISIBLE);
		
		HashMap<String, String> map = applist.get(aPosition);// getItem(aPosition);
		String verse = map.get("section_id");
		verse = verse + " " + map.get("verse");
		verse = verse.trim();
		verse = verse.replace("\n", "").replace("\r", "");
		String verse_key = map.get("section_key");
		verse_key = verse_key + " " + map.get("verse");
		verse_key = verse_key.trim();
		verse_key = verse_key.replace("\n", "").replace("\r", "");
		
		
/**  Use this code when there is need to show selected verse while showing bible    **/
		
		if (position > -1) {
			if (position == aPosition )
			{
				holder.txtVerse.setBackgroundColor(mContext.getResources()
						.getColor(R.color.Wheat));
			}
			if (aPosition != position){
				holder.txtVerse.setBackgroundColor(mContext.getResources()
						.getColor(R.color.white));
			}
				
		}
		
/**-----------------------------------------------------------------------------------**/	
        
        if(isArrowShow)
        {
        	holder.txtVerse.setText(verse_key);
        }else{
        	holder.txtVerse.setText(verse);	
        }
      
        
        

		if (count > 0) {
			if (aPosition == 0) {
				viewHolder.setBackgroundResource(R.drawable.top_corner_bg);
			} else if (aPosition == count) {
				viewHolder.setBackgroundResource(R.drawable.bottom_corner_bg);
			} else {
				viewHolder.setBackgroundResource(R.drawable.middle_bg);
			}
		}

		return viewHolder;

	}

	class ViewHolder {
		TextView txtVerse;
		ImageView imageArrow;
		LinearLayout layoutBibleCell,layoutbottm, layouttop;
		JustifiedTextView textVJustify;
		int id;
	}
}
