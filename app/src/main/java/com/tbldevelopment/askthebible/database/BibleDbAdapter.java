package com.tbldevelopment.askthebible.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.tbldevelopment.askthebible.Utility;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class BibleDbAdapter extends SQLiteOpenHelper {

	private static final String DB_NAME = "Kjv_Bible.sqlite";
	private static final String ID = "_id";
	private static final String CHAPTER_NAME = "chapte_name";
	private static final String SECTION_ID = "section_id";
	private static final String SECTION = "section";
	private static final String VERSE = "verse";
	private static final String BIBLE_TABLE = "Kjv_Bible";
	private static final String BIBLE_TABLE_WEB = "Kjv_Bible_web";
	private static final String KEYWORD_TABLE = "Keyword_Bible";
	private static final String KEYWORD = "keyword";
	private static final String CHAPTERS = "chapter_info";
	private static final String BIBLETOPIC = "bible_title";
	private Context mContext;
	private String tableName;
	
	
	SQLiteDatabase db;

	public BibleDbAdapter(Context context) {
		super(context, DB_NAME, null, 1);
		// TODO Auto-generated constructor stub
		mContext = context;
		if(Utility.getSharedPreferences(mContext, "bible")==null)
			Utility.setSharedPreference(mContext, "bible", "Kjv_Bible");
		tableName = Utility.getSharedPreferences(mContext, "bible");
	}

	public synchronized void close() {
		if (this.getWritableDatabase() != null) {
			this.getWritableDatabase().close();
		}
		super.close();
	}

	public void createDb()
	{
		onCreate(getWritableDatabase());
	}
	
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_Bibles_table = "CREATE TABLE Kjv_Bible (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, chapte_name VARCHAR, section_id INTEGER, section VARCHAR, verse VARCHAR)";
		String CREATE_Keywrod_table = "CREATE TABLE Keyword_Bible (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, keyword VARCHAR, chapter_info VARCHAR)";
		String CREATE_Bibles_web_table = "CREATE TABLE Kjv_Bible_web (_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, chapte_name VARCHAR, section_id INTEGER, section VARCHAR, verse VARCHAR)";
		
		db.execSQL(CREATE_Bibles_table);
		db.execSQL(CREATE_Bibles_web_table);
		db.execSQL(CREATE_Keywrod_table);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// db.execSQL("DROP TABLE IF EXISTS Kjv_Bible");
		// db.execSQL("DROP TABLE IF EXISTS Keyword_Bible");
		// Create tables again
		// onCreate(db);
	}

	@SuppressWarnings("rawtypes")
	public long insertBibleRecord(HashMap<String, String> map) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		Set keySet = map.keySet();
		Iterator<?> keys = keySet.iterator();
		//
		while (keys.hasNext()) {
			final String key = (String) keys.next();
			values.put(key, map.get(key));
		}
		int row = (int) db.insert(tableName, null, values);
		db.close(); // Closing database connection
		
		return row;
	}

	public void updateDatabase(HashMap<String, String> map, String id)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		Set<String> keySet = map.keySet();
		Iterator<?> keys = keySet.iterator();
		//
		while (keys.hasNext()) {
			final String key = (String) keys.next();
			values.put(key, map.get(key));
		}
		db.update(BIBLE_TABLE_WEB, values, "_id=" + id, null);
		db.close();
	}
	
	@SuppressWarnings("rawtypes")
	public long insertKeywordRecord(HashMap<String, String> map) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();		

		int row=0 ;
			String keyword = map.get("keyword");
			String chapter_info = map.get("chapter_info");
			Log.e("from database keyword:--",keyword);
			Log.e("from database chapter:--",chapter_info);
			values.put("chapter_info", chapter_info);
			values.put("keyword", keyword);
			row= (int) db.insert(KEYWORD_TABLE, null, values);
		db.close(); // Closing database connection		
		return row;
		
	}

	// Get Book info from keyword
	public String getBiblesFromKeyword(String keyword) {
		String info = null;
		SQLiteDatabase db = this.getReadableDatabase();
		keyword = keyword.replace("\r", "");
		Cursor cursor = null;
		String query = "SELECT * FROM keyword_Bible where keyword=?";
		cursor = db.rawQuery(query, new String[] { keyword });
				//db.query(KEYWORD_TABLE, new String[] { KEYWORD, CHAPTERS },
				//"keyword=?", new String[] { keyword }, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				info = cursor.getString(cursor.getColumnIndex(CHAPTERS));
			}
		}
		cursor.close();
		db.close();
		return info;

	}
	
	/**Get Bible Title from Keyword**/
	public String getBiblesTitlesFromKeyword(String keyword) {
		String info = null;
		SQLiteDatabase db = this.getReadableDatabase();
		keyword = keyword.replace("\r", "");
		Cursor cursor = null;
		String query = "SELECT * FROM Bible_Title where keyword=?";
		cursor = db.rawQuery(query, new String[] { keyword });
				//db.query(KEYWORD_TABLE, new String[] { KEYWORD, CHAPTERS },
				//"keyword=?", new String[] { keyword }, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				info = cursor.getString(cursor.getColumnIndex(BIBLETOPIC));
			}
		}
		cursor.close();
		db.close();
		return info;

	}

	
	
	// Fetch All bibles from it's title
		public ArrayList<HashMap<String, String>> getBiblesWithKvj(String title) {
			ArrayList<HashMap<String, String>> biblelist = new ArrayList<HashMap<String, String>>();
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = null;
			cursor = db.query(tableName, new String[] { ID, CHAPTER_NAME, SECTION_ID,
					SECTION, VERSE }, "chapte_name=?", new String[] { title },
					null, null, null);
			if (cursor != null) {
				if (cursor.moveToFirst()){
					do {
						HashMap<String, String> map = new HashMap<String, String>();
						map.put(ID, String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(ID))));
						map.put(CHAPTER_NAME, cursor.getString(cursor
								.getColumnIndex(CHAPTER_NAME)));
						map.put(SECTION_ID,
								cursor.getString(cursor.getColumnIndex(SECTION_ID)));
						map.put(SECTION, String.valueOf(cursor.getInt(cursor
								.getColumnIndex(SECTION))));
						map.put(VERSE,
								cursor.getString(cursor.getColumnIndex(VERSE)));
						biblelist.add(map);
						map = null;

					} while (cursor.moveToNext());
				}
				cursor.close();
				db.close(); 
			}

			return biblelist;
		}


	public ArrayList<String> getNumberofchapter(String title)
	{
		ArrayList<String> chapterlist = new ArrayList<String>();
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		cursor = db.query(tableName, new String[] {SECTION}, "chapte_name=?", new String[] { title },
				null, null, null);
		if (cursor != null)
		{
			if (cursor.moveToFirst())
			{
				do {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(SECTION, String.valueOf(cursor.getInt(cursor
							.getColumnIndex(SECTION))));
					if(!chapterlist.contains(map.get("section")))
						chapterlist.add(map.get("section"));
					map = null;

				} while (cursor.moveToNext());
			}
			
			cursor.close();
			db.close(); 
		}
		return chapterlist;
		
	}
	
		
	// Fetch All bibles from it's section id and title
		public ArrayList<HashMap<String, String>> getBiblesWithChapter(String chapter, String title) {
			ArrayList<HashMap<String, String>> biblelist = new ArrayList<HashMap<String, String>>();
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = null;
			cursor = db.query(tableName, new String[] { ID, CHAPTER_NAME,
					SECTION_ID, SECTION, VERSE }, "chapte_name=? AND section=?",
					new String[] { title, chapter }, null, null, null);
			
			if (cursor != null) {
				if (cursor.moveToFirst()){
					do {
						HashMap<String, String> map = new HashMap<String, String>();
						map.put(ID, String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(ID))));
						map.put(CHAPTER_NAME, cursor.getString(cursor
								.getColumnIndex(CHAPTER_NAME)));
						map.put(SECTION_ID,
								cursor.getString(cursor.getColumnIndex(SECTION_ID)));
						map.put(SECTION, String.valueOf(cursor.getInt(cursor
								.getColumnIndex(SECTION))));
						map.put(VERSE,
								cursor.getString(cursor.getColumnIndex(VERSE)));
						biblelist.add(map);
						map = null;

					} while (cursor.moveToNext());
				}
				cursor.close();
				db.close(); 
			}

			return biblelist;
		}
		

	// Fetch All bibles from it's title
	public ArrayList<HashMap<String, String>> getBiblesWithTitle(String title) {
		ArrayList<HashMap<String, String>> biblelist = new ArrayList<HashMap<String, String>>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		cursor = db.query(tableName, new String[] { ID, CHAPTER_NAME, SECTION_ID,
				SECTION, VERSE }, "chapte_name=?", new String[] { title },
				null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst()){
				do {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(ID, String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(ID))));
					map.put(CHAPTER_NAME, cursor.getString(cursor
							.getColumnIndex(CHAPTER_NAME)));
					map.put(SECTION_ID,
							cursor.getString(cursor.getColumnIndex(SECTION_ID)));
					map.put(SECTION, String.valueOf(cursor.getInt(cursor
							.getColumnIndex(SECTION))));
					map.put(VERSE,
							cursor.getString(cursor.getColumnIndex(VERSE)));
					biblelist.add(map);
					map = null;

				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close(); 
		}

		return biblelist;
	}

	public int getIndexOFChapter(String chapter, String section) {
		int index = -1;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		cursor = db.query(tableName, new String[] { ID, CHAPTER_NAME,
				SECTION_ID, SECTION, VERSE }, "chapte_name=? AND section_id=?",
				new String[] { chapter, section }, null, null, null);
		if (cursor != null) {
			if(cursor.moveToFirst())
				index = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
			cursor.close();
			db.close(); 
		}
		
		index = index-1;
		return index;
	}

	public ArrayList<String> getAllKeyword() {
		ArrayList<String> list = new ArrayList<String>();

		String result = null;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		cursor = db.query(KEYWORD_TABLE, new String[] { KEYWORD, CHAPTERS },
				null, null, null, null, null);
		int i = 0;
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					result = cursor.getString(cursor.getColumnIndex(KEYWORD));
					list.add(result);
					i = i + 1;
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close(); 
		}

		return list;
	}

	public ArrayList<String> searchKeyWord(String key) {
		ArrayList<String> list = new ArrayList<String>();
		
		String result = null;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		cursor = db.query(KEYWORD_TABLE, new String[] { KEYWORD, CHAPTERS },
				"keyword LIKE ?", new String[] { "%" + key + "%" }, null, null,
				null);
		int i = 0;
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					result = cursor.getString(cursor.getColumnIndex(KEYWORD));
					list.add(result);
					i = i + 1;
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close(); 
		}

		return list;
	}

	public HashMap<String, String> getBibleFromChapter(String bookTitle,
			String chapterNumber) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		if (bookTitle == null || chapterNumber == null) {
			Log.d("", "book " + bookTitle + "num " + chapterNumber);
			return null;
		}
		cursor = db.query(tableName, new String[] { CHAPTER_NAME, SECTION_ID,
				SECTION, VERSE }, "chapte_name=? AND section_id=?",
				new String[] { bookTitle, chapterNumber }, null, null, null);
		HashMap<String, String> map = new HashMap<String, String>();
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {

					map.put(CHAPTER_NAME, cursor.getString(cursor
							.getColumnIndex(CHAPTER_NAME)));
					map.put(SECTION_ID,
							cursor.getString(cursor.getColumnIndex(SECTION_ID)));
					map.put(SECTION, String.valueOf(cursor.getInt(cursor
							.getColumnIndex(SECTION))));
					map.put(VERSE,
							cursor.getString(cursor.getColumnIndex(VERSE)));

				} while (cursor.moveToNext());
			} else {
				map = null;
			}
			cursor.close();
			db.close(); 
		}

		return map;
	}
}
