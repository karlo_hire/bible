package com.tbldevelopment.askthebible;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("DefaultLocale")
public class TopicByAlphaScreenActivity extends Activity{
	
	private LinearLayout layoutProgress;
	private Context mContext;
	private ApplicationStringAdapter adapterString;
	private ArrayList<String> listOfContents;
	private ListView listView;
	private boolean isLoaded;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.right_fragment_list);
		mContext = this;
		final ActionBar bar = getActionBar();
		String title = this.getIntent().getExtras().getString("alpha");
		title = title+" Topics";
		bar.setTitle(title);
		
		// Progress bar layout
		layoutProgress = (LinearLayout)findViewById(R.id.layoutProgress);
		
		listOfContents = new ArrayList<String>();
		adapterString = new ApplicationStringAdapter(mContext, -1,
				R.layout.cell_layout, listOfContents);
		
		 listView = (ListView) findViewById(R.id.listViewCommon);
		 
		 listView.setAdapter(adapterString);
		 listView.setOnItemClickListener(new OnItemClickListener() {

				@SuppressLint("DefaultLocale")
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					// TODO Auto-generated method stub
						String keyword = listOfContents.get(arg2);
						//keyword = keyword.toLowerCase();
						BibleDbAdapter adapter = new BibleDbAdapter(mContext);
						Intent intent = new Intent(mContext,
								BibleByKeywordActivity.class);
						intent.putExtra("bible", listOfContents.get(arg2));
						String key = adapter.getBiblesFromKeyword(keyword);
						intent.putExtra("title", bar.getTitle());
						intent.putExtra("key", key);
						startActivity(intent);
			
				}
			});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(!isLoaded)
		{
			isLoaded = true;
			String alpha = this.getIntent().getStringExtra("alpha");
			new GetTopics().execute(alpha);
		}
	}

	public class GetTopics extends AsyncTask<String, Void, String> {
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			layoutProgress.setVisibility(View.VISIBLE);
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String displayText = "";
			int resourcesOfTopics = R.raw.keywords_and_phrases;
			
			//for (int i = 0; i < resourcesOfTopics.length; i++) {
				try {
					InputStream fileStream = null;
					fileStream = getResources().openRawResource(
							resourcesOfTopics);
					int fileLen = fileStream.available();
					// Read the entire resource into a local byte buffer.
					byte[] fileBuffer = new byte[fileLen];
					fileStream.read(fileBuffer);
					fileStream.close();
					displayText = new String(fileBuffer);
				} catch (IOException e) {
					// exception handling
				}

				String[] tabOfShortString = displayText.split("\n");

				for (String strtitle : tabOfShortString) {
					if(strtitle.toLowerCase().startsWith(params[0].toLowerCase()))
						listOfContents.add(strtitle);
				}
			//}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			adapterString.notifyDataSetChanged();
		}
	}
}
