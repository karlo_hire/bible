/* Here Is Right side fragment used in tablet version purpose of this class is,
 * it is used firstly to show either list of topics or setting list depending on 
 * the value of "requestFor". Also name of title bar or action bar is set here replaced 
 * in ManiActivity according to, overlapping "PANE" request which means other fragments. */
package com.tbldevelopment.askthebible;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class RightFragment extends Fragment {
	String[] textEntries;
	int requestFor = 0;
	String displayText = "";
	View mainview;
	private LayoutInflater lflater;
	private Typeface mFace;
	private ArrayList<String> listOfContents;
	private ApplicationStringAdapter adapterString;
	private ProgressBar progressBar;
	private LinearLayout layoutProgress;
	private ListView listView;
	private Context appContext;
	private  ActionBar bar;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		appContext=getActivity();
		requestFor = Utility.getSharedPreferences1(getActivity(), "Pref");


		
//if (getArguments()!=null) {
////	requestFor = Utility.getSharedPreferences1(getActivity(), "Pref");
//	requestFor=getArguments().getInt("requestFor");
//}else{
////  requestFor=Constant.GOD;
//	requestFor = Utility.getSharedPreferences1(getActivity(), "Pref");
//
//}
		if (requestFor == Constant.SETUP) {
			
			mainview = inflater.inflate(R.layout.right_fragment_list_setting, container,
					false);
			layoutProgress = (LinearLayout)getActivity().findViewById(R.id.layoutProgress);

		} else {
			mainview = inflater.inflate(R.layout.right_fragment_list, container,
					false);
			layoutProgress = (LinearLayout)getActivity().findViewById(R.id.layoutProgress);
			
		}
	
		mFace = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/verdana.ttf");
		
		lflater = (LayoutInflater) getActivity().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		listOfContents = new ArrayList<String>();

		adapterString = new ApplicationStringAdapter(getActivity(), -1,
				R.layout.cell_layout, listOfContents);
		layoutProgress = (LinearLayout)mainview.findViewById(R.id.layoutProgress);
		progressBar = (ProgressBar)mainview.findViewById(R.id.progressBarCategory);
		listView = (ListView) mainview.findViewById(R.id.listViewCommon);
				
		if(listView!=null ){
			
//		progressBar.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.spinner));
			
		listView.setAdapter(adapterString);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressLint("DefaultLocale")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if (requestFor == Constant.SETUP) {

				} else {
					listView.setVisibility(View.GONE);
					String keyword = listOfContents.get(arg2);
//					keyword = keyword.toLowerCase();
//					Intent intent = new Intent(getActivity(),
//							BibleByKeywordFragment.class);
					Intent intent = new Intent("ClickLoadPage");
					
					BibleDbAdapter adapter = new BibleDbAdapter(getActivity());
					intent.putExtra("bible", listOfContents.get(arg2));
					intent.putExtra("PANE", Constant.KEYDETAIL);
					intent.putExtra("title", bar.getTitle());
					String key = adapter.getBiblesFromKeyword(keyword);
						intent.putExtra("key", key);
						
					LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
				}

			}
		});
		}
		setOptionData(requestFor);

		return mainview;
	}
	
	
@Override
public void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	if (listOfContents.size()>0) {
		if(listView!=null)
		listView.setVisibility(View.VISIBLE);
	}
}
	public void setOptionData(int request) throws InflateException {
		System.out.println(request);

		bar = getActivity().getActionBar();
		if (requestFor == Constant.RELATIONSHIP) {
			bar.setTitle("Relationships and Sex");
		} else if (requestFor == Constant.HEALTH) {
			bar.setTitle("Health and Your Body");
		} else if (requestFor == Constant.MONEY) {
			bar.setTitle("Money and Work");
		} else if (requestFor == Constant.FEELINGS) {
			bar.setTitle("Feelings and Emotions");
		} else if (requestFor == Constant.GOD) {
			bar.setTitle("God and spirituality");
		} else if (requestFor == Constant.SETUP) {
			bar.setTitle("Setup and Info");
			for (String settings : Constant.SETUPNINFO) {
				listOfContents.add(settings);
			}
		} else if (requestFor == Constant.BIBLE_KEYWORD_PHRASES) {
			bar.setTitle("Keyword And Phrases");

		}
		if (requestFor == 0) 
			requestFor = Constant.HEALTH;
		else 
			requestFor = request;
		if (requestFor != Constant.SETUP && requestFor != Constant.BIBLE
				&& requestFor != Constant.BIBLE_KEYWORD_PHRASES) {
			lflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			new GetTopics().execute();

		}// if ends

		// if request not for health,relation,feelings,money
		else if (requestFor == Constant.SETUP) {
			System.out.println("REQFOR" + requestFor);
			try {
				lflater = (LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				final LinearLayout ll = (LinearLayout) mainview
						.findViewById(R.id.layoutOption);
				ll.removeAllViews();
				for (int i = 0; i < Constant.SETUPNINFO.length; i++) {
//					 System.out.println(textEntries[i]);
					 try{
					final LinearLayout holder = (LinearLayout) lflater.inflate(
							R.layout.cell_layout, null);
					 
					TextView txtTitle = (TextView) holder
							.findViewById(R.id.txtTitle);
					holder.setTag(i);
					txtTitle.setText(Constant.SETUPNINFO[i]);
					txtTitle.setTypeface(Utility.getTypeFace(appContext));
					if (i == (Constant.SETUPNINFO.length - 1)) {
						((LinearLayout) holder.findViewById(R.id.cell_seprator))
								.setVisibility(View.GONE);
					}

					ll.addView(holder);
					 
					holder.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							
							if (v.getTag().toString().equals("1")) {
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setData(Uri
										.parse("market://details?id=com.PandoraTV"));
								if (MyStartActivity(intent) == false) {
									// Market (Google play) app seems not installed,
									// let's try to open a webbrowser
									intent.setData(Uri
											.parse("https://play.google.com/store/apps/details?id=com.PandoraTV"));
				                   
								}
								 startActivity(intent);
							}
							if(v.getTag().toString().equals("3")){
								Intent email = new Intent(Intent.ACTION_SEND);
								email.putExtra(
										Intent.EXTRA_EMAIL,
										new String[] { "feedback@askthebibleapp.com" });
								email.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
								email.setType("message/rfc822");
								startActivity(Intent.createChooser(email,
										"Choose an Email client :"));
							}
							if(v.getTag().toString().equals("2")){
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setData(Uri
										.parse("https://www.askthebibleapp.com"));
				               
								startActivity(intent);
								
							}if(v.getTag().toString().equals("4")){
							ll.removeAllViews();
							Intent intent = new Intent("ClickLoadPage");
							int set = Integer.parseInt(holder.getTag().toString());
							intent.putExtra("requestFor", set);
							intent.putExtra("TAG", holder.getTag().toString());
							intent.putExtra("PANE",Constant.ABOUTUS);
							LocalBroadcastManager.getInstance(getActivity())
									.sendBroadcast(intent);
							}if(v.getTag().toString().equals("0")){
							ll.removeAllViews();
							Intent intent = new Intent("ClickLoadPage");
							int set = Integer.parseInt(holder.getTag().toString());
							intent.putExtra("requestFor", set);
							intent.putExtra("TAG", holder.getTag().toString());
							intent.putExtra("PANE",Constant.SETUPINFO);
							LocalBroadcastManager.getInstance(getActivity())
									.sendBroadcast(intent);
								
							}
							if(v.getTag().toString().equals("5")){
								ll.removeAllViews();
								Intent intent = new Intent("ClickLoadPage");
								int set = Integer.parseInt(holder.getTag().toString());
								intent.putExtra("requestFor", set);
								intent.putExtra("TAG", holder.getTag().toString());
								intent.putExtra("PANE",Constant.SCRIPTURE);
								LocalBroadcastManager.getInstance(getActivity())
										.sendBroadcast(intent);
							}
						}
					});

					 }catch(RuntimeException e){
						 e.printStackTrace();
					 }
				}
			} catch (InflateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	public class GetTopics extends AsyncTask<Void, Void, String> {

		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
//			mProgressDialog = ProgressDialog.show(getActivity().getApplicationContext(), "Please wait",
//					"Loading...");
//			mProgressDialog.setCancelable(true);
			layoutProgress.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
			listOfContents.clear();
		}

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
		

			try {
				InputStream fileStream = null;
				if (requestFor == Constant.RELATIONSHIP) {
					fileStream = getResources().openRawResource(
							R.raw.relationships);
				}
				if (requestFor == Constant.HEALTH) {
					fileStream = getResources().openRawResource(R.raw.health);
				}
				if (requestFor == Constant.MONEY) {
					fileStream = getResources().openRawResource(R.raw.money);
				}
				if (requestFor == Constant.FEELINGS) {
					fileStream = getResources().openRawResource(R.raw.feelings);
				}
				if(requestFor == Constant.GOD)
				{
					fileStream = getResources().openRawResource(R.raw.god);
				}
				int fileLen = fileStream.available();
				// Read the entire resource into a local byte buffer.
				byte[] fileBuffer = new byte[fileLen];
				fileStream.read(fileBuffer);
				fileStream.close();
				displayText = new String(fileBuffer);
			} catch (IOException e) {
				// exception handling
			}

			String[] tabOfShortString = displayText.split("\n");

			for (String strtitle : tabOfShortString) {
				listOfContents.add(strtitle);
			}
			// int length = tabOfShortString.length;
			// System.out.println("Length of float string is" + length);
			// textEntries = new String[length];
			// for (int l = 0; l < length; l++) {
			// String res = new String(tabOfShortString[l]);
			// textEntries[l] = res.trim();
			// }

			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			// LinearLayout ll = (LinearLayout) findViewById(R.id.layoutOption);
			// //ll.setBackgroundResource(R.drawable.corner_style);
			// for (int i = 0; i < textEntries.length; i++) {
			// // System.out.println(textEntries[i]);
			// final LinearLayout holder = (LinearLayout) lflater.inflate(
			// R.layout.cell_layout, null);
			// TextView txtTitle = (TextView)
			// holder.findViewById(R.id.txtTitle);
			// holder.setTag(textEntries[i]);
			// //holder.setBackgroundResource(R.drawable.corner_style);
			// txtTitle.setText(textEntries[i]);
			// txtTitle.setTypeface(mFace);
			// if (i == (textEntries.length - 1)) {
			// ((LinearLayout) holder.findViewById(R.id.cell_seprator))
			// .setVisibility(View.GONE);
			// }
			//
			// ll.addView(holder);
			// holder.setOnClickListener(new View.OnClickListener() {
			//
			// @Override
			// public void onClick(View v) {
			// // TODO Auto-generated method stub
			// String keyword = (String)v.getTag();
			// keyword = keyword.toLowerCase();
			// BibleDbAdapter adapter = new BibleDbAdapter(appContext);
			// Intent intent = new Intent(appContext,
			// BibleByKeywordActivity.class);
			// intent.putExtra("bible", (String)v.getTag());
			// if (requestFor == Constant.RELATIONSHIP) {
			// String key = adapter.getBiblesFromKeyword(keyword);
			// intent.putExtra("key", key);
			// }
			// if (requestFor == Constant.HEALTH) {
			// String key = adapter.getBiblesFromKeyword(keyword);
			// intent.putExtra("key", key);
			// }
			// if (requestFor == Constant.MONEY) {
			// String key = adapter.getBiblesFromKeyword(keyword);
			// intent.putExtra("key", key);
			// }
			// if (requestFor == Constant.FEELINGS) {
			// String key = adapter.getBiblesFromKeyword(keyword);
			// intent.putExtra("key", key);
			// }
			//
			// startActivity(intent);
			// adapter = null;
			// }
			// });
			//
			// }
			//
			// TODO Auto-generated method stub
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			adapterString.notifyDataSetChanged();
		}
	}
	
	private boolean MyStartActivity(Intent aIntent) {
		try {
			startActivity(aIntent);
			return true;
		} catch (ActivityNotFoundException e) {
			return false;
		}
	}
}