/* Used in tablet version it states left side of tablet 
 * and contains a list of Health and Body,Money and Work,
 * Feelings and Emotions etc.
 *       It then takes the most important int Variable "requestFor" 
 * at MainActivity and then set fragment argument there*/
package com.tbldevelopment.askthebible;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class LeftFragment extends Fragment {

	private Typeface mFace;
	private Context appContext;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_left_pane,
				container, false);
		appContext = getActivity();
//		mFace = Typeface.createFromAsset(getActivity().getAssets(),
//				"fonts/verdana.ttf");

		LinearLayout ll_health = (LinearLayout) view
				.findViewById(R.id.layoutHealthYourBody);
		LinearLayout ll_relationship = (LinearLayout) view
				.findViewById(R.id.layoutRelationAndS);
		LinearLayout ll_money = (LinearLayout) view
				.findViewById(R.id.layoutMoneyAndWork);
		LinearLayout ll_feelings = (LinearLayout) view
				.findViewById(R.id.layoutFeelingsAndEmotions);
		LinearLayout ll_keywords = (LinearLayout) view
				.findViewById(R.id.layoutKeywordsAndPhrase);
		LinearLayout ll_bible = (LinearLayout) view
				.findViewById(R.id.layoutBible);
		LinearLayout ll_setup = (LinearLayout) view
				.findViewById(R.id.layoutSetupAndInfo);
		LinearLayout ll_god = (LinearLayout) view
				.findViewById(R.id.layoutGodAndSpirit);

		TextView tv_health = (TextView) view.findViewById(R.id.textVhealth);
		TextView tv_relationship = (TextView) view
				.findViewById(R.id.textVrelation);
		TextView tv_money = (TextView) view.findViewById(R.id.textVMoney);
		TextView tv_feelings = (TextView) view.findViewById(R.id.textVfeelings);
		TextView tv_keywords = (TextView) view.findViewById(R.id.textVkeywords);
		TextView tv_bible = (TextView) view.findViewById(R.id.txtVbible);
		TextView tv_setup = (TextView) view.findViewById(R.id.txtVsetup);
		TextView tv_god = (TextView) view.findViewById(R.id.textGodSpirit);

		tv_health.setTypeface(Utility.getTypeFace(appContext));
		tv_relationship.setTypeface(Utility.getTypeFace(appContext));
		tv_money.setTypeface(Utility.getTypeFace(appContext));
		tv_feelings.setTypeface(Utility.getTypeFace(appContext));
		tv_keywords.setTypeface(Utility.getTypeFace(appContext));
		tv_bible.setTypeface(Utility.getTypeFace(appContext));
		tv_setup.setTypeface(Utility.getTypeFace(appContext));
		tv_god.setTypeface(Utility.getTypeFace(appContext));

		ll_god.setOnClickListener(onclickListener);
		ll_health.setOnClickListener(onclickListener);
		ll_relationship.setOnClickListener(onclickListener);
		ll_money.setOnClickListener(onclickListener);
		ll_feelings.setOnClickListener(onclickListener);
		ll_keywords.setOnClickListener(onclickListener);
		ll_bible.setOnClickListener(onclickListener);
		ll_setup.setOnClickListener(onclickListener);
		/*
		 * ll.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { updateDetail(); } });
		 */

		return view;
	}

	OnClickListener onclickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			int id = v.getId();
			boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
			Intent intent = new Intent("ClickLoadPage");
			switch (id) {

			case R.id.layoutRelationAndS:
				TextView txtVrelation = (TextView) v
						.findViewById(R.id.textVrelation);
				// String relation = txtVrelation.getText().toString();
				intent.putExtra("PANE", "left");
				intent.putExtra("requestFor", Constant.RELATIONSHIP);
				intent.putExtra("HEADING", "Relationship and Sex");
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
				break;

			case R.id.layoutHealthYourBody:
				intent.putExtra("PANE", "left");
				intent.putExtra("requestFor", Constant.HEALTH);
				intent.putExtra("HEADING", "Health And Your Body ");
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
				break;

			case R.id.layoutGodAndSpirit: {
				intent.putExtra("PANE", "left");
				intent.putExtra("requestFor", Constant.GOD);
				intent.putExtra("HEADING", "God and spirituality");
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
			}
				break;

			case R.id.layoutMoneyAndWork:
				intent.putExtra("PANE", "left");
				intent.putExtra("requestFor", Constant.MONEY);
				intent.putExtra("HEADING", "Money And Work");
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
				break;

			case R.id.layoutFeelingsAndEmotions:
				intent.putExtra("PANE", "left");
				intent.putExtra("requestFor", Constant.FEELINGS);
				intent.putExtra("HEADING", "Feelings And Emotions");
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
				break;

			case R.id.layoutKeywordsAndPhrase:
				TextView txtVkeywords = (TextView) v
						.findViewById(R.id.textVkeywords);
				// String keywords = txtVkeywords.getText().toString();
				intent.putExtra("requestFor", Constant.BIBLE_KEYWORD_PHRASES);
				intent.putExtra("HEADING", "Keyword And Phrases");
				intent.putExtra("PANE", Constant.KEYPHASES);
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
				break;

			case R.id.layoutBible:
				intent.putExtra("PANE", "left");
				intent.putExtra("requestFor", Constant.BIBLE);
				intent.putExtra("HEADING", "bible");
				intent.putExtra("PANE", Constant.BIBLETOPIC);
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
				break;

			case R.id.layoutSetupAndInfo:
				intent.putExtra("PANE", "left");
				intent.putExtra("requestFor", Constant.SETUP);
				intent.putExtra("TAG", "1");
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
						intent);
				break;

			}

		}
	};

	// May also be triggered from the Activity
	public void updateDetail(String heading) {
		// Create fake data
		// String newTime = String.valueOf(System.currentTimeMillis());
		// Send data to Activity
		// listener.onRssItemSelected(heading);

	}
}