package com.tbldevelopment.askthebible;

import java.util.ArrayList;

import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

public class BibleTopicsActivity extends Activity {
	private Context appContext;
	private String xml = "";
	private ProgressDialog mProgressDialog;
	private ApplicationStringAdapter adapterString;
	private String[] bibles_title;
	private ArrayList<String> listoftopics;
	private LinearLayout layoutProgress;
	private ListView listView;
	private boolean isKjv = false;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bible_list);
		appContext = this;
		ActionBar bar = getActionBar();
		isKjv = Utility.getSharedPreferences2(appContext, Constant.BIBLEVERSION[1]);
		if(isKjv){
			bibles_title = getResources().getStringArray(R.array.bible_kjv_book_title);
			bar.setTitle("King James Version");
		}
		else{
			bibles_title = getResources().getStringArray(R.array.bible_web_book_title);
			bar.setTitle("World English Bible");

		}
		
		listoftopics = new  ArrayList<String>();
		listView = (ListView)findViewById(R.id.listViewBibletopic);
		layoutProgress = (LinearLayout)findViewById(R.id.layoutProgress);
		adapterString = new ApplicationStringAdapter(appContext, -1, R.layout.cell_layout, listoftopics);
		
		
		listView.setAdapter(adapterString);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int tag,
					long arg3) {
				// TODO Auto-generated method stub
				String topic = bibles_title[tag];
				final String value = listoftopics.get(tag);
				Intent in = new Intent(appContext, BibleBooksChapterRangeActivity.class);
				in.putExtra("title", value);
				in.putExtra("bible", topic);
				in.putExtra("requestFor", Constant.BIBLE);
				startActivity(in);
				
			}
		});
		//mProgressDialog = ProgressDialog.show(appContext, "Please Wait...", "Loading Topics");
		layoutProgress.setVisibility(View.VISIBLE);
		listView.setVisibility(View.GONE);
		Thread th = new Thread(loadTopics);
		th.start();
	}
	
	
	

	
	protected void onResume() {
	super.onResume();
	
	}

	
	
	
	Runnable loadTopics = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			
			createTopicList();
		}
	};
	
	private void createTopicList() {
		
		String[] topics =null;
		if(isKjv)
			topics	= getResources().getStringArray(R.array.bible_list);
		else
			topics = getResources().getStringArray(R.array.bible_list_web);
		
		for(String key : topics)
		{
			listoftopics.add(key);
		}
		
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					layoutProgress.setVisibility(View.GONE);
					listView.setVisibility(View.VISIBLE);
					adapterString.notifyDataSetChanged();
				}
			});
		
		
		
	}

}
