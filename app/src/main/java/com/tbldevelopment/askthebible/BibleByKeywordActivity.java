package com.tbldevelopment.askthebible;

import java.util.ArrayList;
import java.util.HashMap;

import com.tbldevelopment.askthebible.database.ApplicationAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class BibleByKeywordActivity extends Activity {

	// Activity Context
	private Context mContext;
	// Progress dialog to show porcessing
	private ProgressDialog mProgressDialog;
	//Array list to store bibles data
	private ArrayList<HashMap<String, String>> listOfBibles;
	// Custom adapter class
	private ApplicationAdapter adapter;
	
	private HashMap<String, String> mapkeys;
	// Check data loaded or not
	private boolean isLoaded;
	
	private LayoutInflater lflater;
	private Typeface mFace;
	private LinearLayout layoutProgress;
	private ListView lv; 
	private boolean isKjv=false;
	String[] web_bibles_title =null;
	String[] kjv_bibles_title=null;
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bible_detail);
		mContext = this;
		mFace =  Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Aver.ttf");
		isKjv = Utility.getSharedPreferences2(mContext, Constant.BIBLEVERSION[1]);
		 String title = this.getIntent().getStringExtra("title");
		
		if(title.contains("Health and Your Body")){
		    title = "Health..";	
		}
		if(title.contains("Relationships and Sex")){
			title = "Rel..";	
			}
		if(title.contains("God and spirituality")){
			title = "God..";	
			}
		if(title.contains("Money and Work")){
			title = "Money..";	
			}
		if(title.contains("Feelings and Emotions")){
			title = "Feel..";	
			}
		else
		{
			//title = this.getIntent().getStringExtra("title");
		}
		final String bookTitle=this.getIntent().getStringExtra("bible");
		final String actionBarTitle= title+">"+bookTitle;
		// title = "Kings James Version " + title;
		String[] kjvKeyswords  = getResources().getStringArray(R.array.bible_kjv_keywords);
		String[] webKeyswords = getResources().getStringArray(R.array.bible_web_keywords);
		Log.d("", "count keyword "+webKeyswords.length);
		//set title according to bible version web/kjv
		if(isKjv){
		kjv_bibles_title = getResources().getStringArray(R.array.bible_kjv_book_title);
		Log.d("", "count web "+kjv_bibles_title.length);
		}
		else{
		 web_bibles_title = getResources().getStringArray(R.array.bible_web_book_title);
		 Log.d("", "count web "+web_bibles_title.length);
		}
		mapkeys = new HashMap<String, String>();
		
		ActionBar actionBar = getActionBar(); 
		actionBar.setTitle(actionBarTitle);
		
		//Put a bible title in the respect of bible key --
		if(isKjv)
		{
		
			for (int j = 0; j < kjvKeyswords.length; j++) {
				String key = kjvKeyswords[j];
				mapkeys.put(key, kjv_bibles_title[j]);
		}
			Log.d("Titles", ""+mapkeys);
		}//Put 
		else
		{
			for (int j = 0; j < webKeyswords.length; j++) {
				String key = webKeyswords[j];
				mapkeys.put(key, web_bibles_title[j]);
		}
		}
		
				
		listOfBibles = new ArrayList<HashMap<String, String>>();
		layoutProgress = (LinearLayout)findViewById(R.id.layoutProgress);
		adapter = new ApplicationAdapter(mContext, -1,
				R.layout.bible_cell_layout, listOfBibles);
		adapter.displayArrow(true);
		lv = (ListView) findViewById(R.id.listViewBibles);
		
		lv.setAdapter(adapter);
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				HashMap<String, String> map = listOfBibles.get(arg2);
				String chapter = map.get("chapte_name");
				String section = map.get("section_id");
				Intent in = new Intent(mContext, BibleDetailActivity.class);
				in.putExtra("sectionid", section);
				in.putExtra("section_key", map.get("section_key"));
				in.putExtra("bible", chapter);
				in.putExtra("title", chapter);
				in.putExtra("book", actionBarTitle);
				in.putExtra("requestFor", Constant.BIBLE_KEYWORD);
				startActivity(in);
			}
		});
		// justifiedTextView = new JustifiedTextView(mContext);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(!isLoaded){
			String key = this.getIntent().getStringExtra("key");
			new SearchContentOfTopic().execute(key);
		}
	}

	public class SearchContentOfTopic extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
//			mProgressDialog = ProgressDialog.show(mContext, "Loading Content",
//					"Searching...");
//			mProgressDialog.setCanceledOnTouchOutside(false);
//			mProgressDialog.setCancelable(true);
			layoutProgress.setVisibility(View.VISIBLE);
			lv.setVisibility(View.GONE);
			listOfBibles.clear();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			//keys exa. {Rom.8.12,Gen.4.2}
			String key = params[0];
			if(key==null)
				return null;
			String[] keys = key.split(",");// spliting by the comma
			ArrayList<HashMap<String, String>> listSearch = new ArrayList<HashMap<String, String>>();
			if (keys.length > 0) {
				for (int i = 0; i < keys.length; i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					if (keys[i].contains("-")) {// check if contain exam {Rom.8.12-Rom.8.16}
						String[] keys2 = keys[i].split("-");
						String[] keysection1 = keys2[0].split("\\.");
						String[] keySection = keys2[1].split("\\.");
						String section = null;
						String section_key = null;
						
						int from = Integer.parseInt(keysection1[2].replace("\\", ""));
						int to = Integer.parseInt(keySection[2].replace("\\", ""));
//						for (int n = from; n <= to; n++) {
							HashMap<String, String> mapsection = new HashMap<String, String>();
							mapsection.put("book", keysection1[0]);
							if (section == null) {
								section = "{"+ keySection[1];
							} 
							if (section_key== null){
								section_key="{"+keySection[0]+":"+keySection[1]+":"+from+"}";
							}
							
							section = section + ":" + from + "}";  // from = int n, I replaced for test.
							mapsection.put("section", section);
							mapsection.put("section_key", section_key);
							//section = null;
							listSearch.add(mapsection);
							//mapsection = null;
					//	}
				
					} else {
						 String strkey = keys[i];
						 String[] keySection =strkey.split("\\.");
						 map.put("book", keySection[0]);
						String section_key = null;
						String section = null;
						//for (int n = 1; n < keySection.length; n++) {
							if (section_key == null) {
								section_key = "{"+keySection[0]+":"+ keySection[1]+":"+keySection[2]+"}";
							}
							if(section==null){
								section="{"+keySection[1]+":"+keySection[2]+"}";
							}
//							} else {
//								//section = section + ":" + keySection[n].replace("\\", "") + "}";
//							}
						//}
						map.put("section", section);
						map.put("section_key", section_key);
						listSearch.add(map);
					}

					
				}
				
			}
			BibleDbAdapter adapter = new BibleDbAdapter(mContext);
			for (int i = 0; i < listSearch.size(); i++) {
				HashMap<String, String> map = listSearch.get(i);
				HashMap<String, String> dataMap = adapter.getBibleFromChapter(
						mapkeys.get(map.get("book")), map.get("section"));
				
				if(dataMap!=null){
				dataMap.put("section_key", map.get("section_key"));	
				listOfBibles.add(dataMap);
				}
			}
			String result = "Success";
			return result;
		}

		protected void onPostExecute(String result) {
			isLoaded = true;
			layoutProgress.setVisibility(View.GONE);
			lv.setVisibility(View.VISIBLE);
			
			if (result != null) {
				if (result.contains("Success")) {
					adapter.notifyDataSetChanged();
				}
				// adapter.notifyDataSetChanged();
			}
		}

	}

}
