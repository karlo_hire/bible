package com.tbldevelopment.askthebible;

public class Constant {
	//Pages 
	public static final int RELATIONSHIP=1;
	public static final int HEALTH=2;
	public static final int MONEY=3;
	public static final int FEELINGS=4;
	public static final int GOD=5;
	public static final int SETUP=6;
	public static final int BIBLE=7;
	public static final int BIBLE_SECTION=11;
	public static final int BIBLE_KEYWORD = 8;
	public static final int BIBLE_KEYWORD_PHRASES = 10;
	public static final String[] SETUPNINFO = new String[]{"Bible Version","Post Review","Webpage","Feedback to developer","About","How to interpret Scripture"};
	public static final String[] BIBLEVERSION = new String[]{"World English Bible","King James Bible"};
	public static final String[] BIBLEVERSION_KEY = new String[]{"Kjv_Bible_web","Kjv_Bible"};
	public static final String BIBLETOPIC = "BibleTopic";
	public static final String BIBLEDETAIL = "BibleDetail";
	public static final String BIBLESECTION = "BibleSection";
	public static final String ALPHATOPIC = "ALPHA";
	public static final String KEYDETAIL = "keyDetail";
	public static final String KEYPHASES = "BibleKeyPharases";
	public static final String SETUPINFO = "Setup";
	public static final String ABOUTUS = "Aboutus";
	public static final String SCRIPTURE = "Scripture";

	public static final String BROADCAST = "ClickLoadPage";

}
