package com.tbldevelopment.askthebible;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DetailActivity extends Activity {

    private LayoutInflater lflater;
    private ProgressBar mProgressDialog;
    private String displayText = "";
    private Context appContext;
    private boolean isContentLoad;
    private ArrayList<String> listOfContents;
    private ApplicationStringAdapter adapterString;
    private LinearLayout layoutProgress;
    int requestFor;
    private ListView listView;
    String[] textEntries;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        requestFor = bundle.getInt("requestFor");

        if (requestFor == Constant.SETUP) {
            setContentView(R.layout.right_fragment_list_setting);
        } else {
            setContentView(R.layout.right_fragment_list);
            layoutProgress = (LinearLayout) findViewById(R.id.layoutProgress);
            mProgressDialog = (ProgressBar) findViewById(R.id.progressBarCategory);
        }
        appContext = this;


        listOfContents = new ArrayList<String>();
// Set Title according to Requested list
        final ActionBar bar = getActionBar();
        if (requestFor == Constant.RELATIONSHIP) {
            bar.setTitle("Relationships and Sex");
        } else if (requestFor == Constant.HEALTH) {
            bar.setTitle("Health and Your Body");
        } else if (requestFor == Constant.MONEY) {
            bar.setTitle("Money and Work");
        } else if (requestFor == Constant.FEELINGS) {
            bar.setTitle("Feelings and Emotions");
        } else if (requestFor == Constant.GOD) {
            bar.setTitle("God and spirituality");
        } else if (requestFor == Constant.SETUP) {
            bar.setTitle("Setup and Info");
            for (String settings : Constant.SETUPNINFO) {
                listOfContents.add(settings);
            }
        } else if (requestFor == Constant.BIBLE_KEYWORD_PHRASES) {
            bar.setTitle("Keyword And Pharases");

        }

        adapterString = new ApplicationStringAdapter(appContext, -1,
                R.layout.cell_layout, listOfContents);
        listView = (ListView) findViewById(R.id.listViewCommon);
        if (listView != null) {
            listView.setAdapter(adapterString);
            listView.setOnItemClickListener(new OnItemClickListener() {

                @SuppressLint("DefaultLocale")
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                        long arg3) {
                    // TODO Auto-generated method stub
                    if (requestFor == Constant.SETUP) {

                    } else {
                        String keyword = listOfContents.get(arg2);
                        //keyword = keyword.toLowerCase();
                        BibleDbAdapter adapter = new BibleDbAdapter(appContext);
                        Intent intent = new Intent(appContext,
                                BibleByKeywordActivity.class);
                        intent.putExtra("bible", listOfContents.get(arg2));
                        intent.putExtra("title", bar.getTitle());
                        String key = adapter.getBiblesFromKeyword(keyword);
                        intent.putExtra("key", key);
                        startActivity(intent);
                    }

                }
            });
        }
        if (requestFor == 7) {

            Intent intent = new Intent(appContext, BibleTopicsActivity.class);
            startActivity(intent);
            finish();
        }

    }

    @SuppressLint("NewApi")
    private void loadContent() {
        if (requestFor != Constant.SETUP && requestFor != Constant.BIBLE
                && requestFor != Constant.BIBLE_KEYWORD_PHRASES) {
            new GetTopics().execute();

        } else if (requestFor == Constant.SETUP) {
            lflater = (LayoutInflater) getApplicationContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout ll = (LinearLayout) findViewById(R.id.layoutOption);
            for (int i = 0; i < Constant.SETUPNINFO.length; i++) {
                // System.out.println(textEntries[i]);
                final LinearLayout holder = (LinearLayout) lflater.inflate(
                        R.layout.cell_layout, null);
                TextView txtTitle = (TextView) holder
                        .findViewById(R.id.txtTitle);
                holder.setTag(i);
                txtTitle.setText(Constant.SETUPNINFO[i]);
                txtTitle.setTypeface(Utility.getTypeFace(appContext));
                if (i == (Constant.SETUPNINFO.length - 1)) {
                    ((LinearLayout) holder.findViewById(R.id.cell_seprator))
                            .setVisibility(View.GONE);
                }

                ll.addView(holder);
                holder.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int index = (Integer) v.getTag();
                        if (v.getTag().toString().equals("1")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri
                                    .parse("market://details?id=com.PandoraTV"));

                            if (MyStartActivity(intent) == false) {
                                // Market (Google play) app seems not installed,
                                // let's try to open a webbrowser
                                intent.setData(Uri
                                        .parse("https://play.google.com/store/apps/details?id=com.PandoraTV"));

                            }
                            startActivity(intent);
                        } else if (v.getTag().toString().equals("3")) {
                            Intent email = new Intent(Intent.ACTION_SEND);
                            email.putExtra(Intent.EXTRA_EMAIL, new String[]{"feedback@askthebibleapp.com"});
                            email.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                            email.setType("message/rfc822");
                            startActivity(Intent.createChooser(email, "Choose an Email client :"));
                        } else if (v.getTag().toString().equals("2")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri
                                    .parse("http://askthebibleapp.com"));

                            startActivity(intent);

                        } else {
                            if (requestFor == Constant.SETUP && !v.getTag().toString().equals("3")) {
                                Intent intent = new Intent(appContext,
                                        SetupDetailActivity.class);
                                intent.putExtra("TAG", String.valueOf(index));
                                intent.putExtra("title",
                                        Constant.SETUPNINFO[index]);
                                startActivity(intent);
                            }
                        }
                    }
                });

            }
            LinearLayout layoutMain = (LinearLayout) findViewById(R.id.layoutMainOption);
            layoutMain.setBackgroundResource(R.drawable.corner_style);
        } else if (requestFor == Constant.BIBLE_KEYWORD_PHRASES) {
            Intent in = new Intent(appContext, BibleKeywordAndPhrases.class);
            startActivity(in);
            finish();
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (!isContentLoad) {
            isContentLoad = true;
            if (requestFor == 7) {

            } else {
                loadContent();
            }

        }
    }

    public class GetTopics extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            mProgressDialog.setVisibility(View.VISIBLE);
            mProgressDialog.startAnimation(AnimationUtils.loadAnimation(appContext, R.anim.spinner));
            layoutProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub
            lflater = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            try {
                InputStream fileStream;

                if (requestFor == Constant.RELATIONSHIP) {
                    fileStream = getResources().openRawResource(
                            R.raw.relationships);
                } else if (requestFor == Constant.HEALTH) {
                    fileStream = getResources().openRawResource(R.raw.health);
                } else if (requestFor == Constant.MONEY) {
                    fileStream = getResources().openRawResource(R.raw.money);
                } else if (requestFor == Constant.FEELINGS) {
                    fileStream = getResources().openRawResource(R.raw.feelings);
                } else if (requestFor == Constant.GOD) {
                    fileStream = getResources().openRawResource(R.raw.god);
                } else {
                    fileStream = getResources().openRawResource(
                            R.raw.relationships);
                }
                int fileLen = fileStream.available();
                // Read the entire resource into a local byte buffer.
                byte[] fileBuffer = new byte[fileLen];
                fileStream.read(fileBuffer);
                fileStream.close();
                displayText = new String(fileBuffer);
            } catch (IOException e) {
                // exception handling
            }

            String[] tabOfShortString = displayText.split("\n");

            for (String strtitle : tabOfShortString) {
                listOfContents.add(strtitle);
            }
            // int length = tabOfShortString.length;
            // System.out.println("Length of float string is" + length);
            // textEntries = new String[length];
            // for (int l = 0; l < length; l++) {
            // String res = new String(tabOfShortString[l]);
            // textEntries[l] = res.trim();
            // }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            layoutProgress.setVisibility(View.GONE);
            mProgressDialog.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            adapterString.notifyDataSetChanged();
        }
    }

    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}
