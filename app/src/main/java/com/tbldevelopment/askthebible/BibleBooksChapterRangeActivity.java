package com.tbldevelopment.askthebible;

import java.util.ArrayList;

import com.tbldevelopment.askthebible.database.ApplicationStringAdapter;
import com.tbldevelopment.askthebible.database.BibleDbAdapter;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class BibleBooksChapterRangeActivity extends Activity {
	private ApplicationStringAdapter adapterString;
	private Context mContext;
	private ArrayList<String> listOfNumChapter;
	private ListView listView;
	private boolean isDataloaded;
	private String strChapterTitle;
	private LinearLayout layoutProgress;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_bible_list);
		mContext =this;
		
		final ActionBar bar = getActionBar();
		// Check Bible version Prefrences
		final String bookRefrence = this.getIntent().getStringExtra("title");
		boolean isKjv = Utility.getSharedPreferences2(mContext, Constant.BIBLEVERSION[1]);
		String title = null;
		if (isKjv) {
			title = "Bible >"+bookRefrence;
		}else
		{
			title = "Bible >"+bookRefrence;
		}
		bar.setTitle(title);
		
		// Get title of book
		strChapterTitle = this.getIntent().getExtras().getString("bible");
		
		// Progress Layout
		layoutProgress = (LinearLayout)findViewById(R.id.layoutProgress);
		
		// Array list to store all chapterid
		listOfNumChapter = new ArrayList<String>();
		
		//List view
		listView = (ListView)findViewById(R.id.listViewBibletopic);
		
		//String Adapter
		adapterString = new ApplicationStringAdapter(mContext, 2, R.layout.cell_layout, listOfNumChapter);
		
		// Set adapter to list view
		listView.setAdapter(adapterString);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
					String chapter = listOfNumChapter.get(arg2);
					Intent in = new Intent(mContext, BibleDetailActivity.class);
					in.putExtra("title", bar.getTitle());
					in.putExtra("bible", strChapterTitle);
					in.putExtra("chapter", chapter);
					in.putExtra("book", bar.getTitle());
					in.putExtra("requestFor", Constant.BIBLE);
					startActivity(in);
			}
		});
	
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(!isDataloaded)
		{
			isDataloaded = true;
			new LoadContetOfTopic().execute("");
		}
	}
	
	public class LoadContetOfTopic extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			layoutProgress.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
			listOfNumChapter.clear();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String result = null;
			
			BibleDbAdapter adapter = new BibleDbAdapter(mContext);
			listOfNumChapter.addAll(adapter.getNumberofchapter(strChapterTitle));
		
			if (listOfNumChapter != null) {
				if (listOfNumChapter.size() > 0) {
					result = "Success";
				} else {
					result = "No bible for the topic.";
				}
			}
			return result;
		}

		protected void onPostExecute(String result) {
			layoutProgress.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			if (result != null) {
				if (result.contains("Success")) {
					adapterString.notifyDataSetChanged();
				}
				// adapter.notifyDataSetChanged();
			}
		}
	}
}
